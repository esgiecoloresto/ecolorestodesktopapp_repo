package fr.esgi.ecoloresto;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import fr.esgi.ecoloresto.controller.LogView;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application {

	private static Stage primaryStage;
	public BorderPane rootLayout;


	@Override
	public void start(Stage primaryStage) throws MalformedURLException, IOException {
		this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Ecolo'Resto");

        this.primaryStage.setResizable(false);

        initRootLayout();

        showLoginView("LogView.fxml");
	}

	/**
	 * Initialise le RootLayout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
			primaryStage.getIcons().add(new Image(getClass().getResource("./view/assets/pancake.png").toExternalForm()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Affiche la page de login dans le RootLayout.
	 */
	public void showLoginView(String panel) {
		try {
			// Chargement de LoginView.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/"+panel));
			AnchorPane loginView = (AnchorPane) loader.load();

			// Placement de LoginView dans le rootLayout.
			rootLayout.setCenter(loginView);

			// Give the controller access to the main app.
			LogView controller = loader.getController();
			controller.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Affiche la page d'accueil de l'utilisateur
	 */
	//    public void showAccountView() {
	//    	System.out.println("Test");
	//    	showLoginView("AccountView.fxml");
	//    }


	/**
	 * Returns the main stage.
	 * @return
	 */
	public static Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}
}
