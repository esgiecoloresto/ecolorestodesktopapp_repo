package fr.esgi.ecoloresto.controller;

import fr.esgi.ecoloresto.MainApp;
import javafx.fxml.FXML;

public class ProposeDishView {
	MainApp mainApp;
	LogView logView;


    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    @FXML
    public void goToHome(){
    	logView.subscribe();
    }

}
