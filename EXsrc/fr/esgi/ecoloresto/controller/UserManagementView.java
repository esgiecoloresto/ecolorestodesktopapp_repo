package fr.esgi.ecoloresto.controller;

import java.net.URL;
import java.util.ResourceBundle;

import fr.esgi.ecoloresto.MainApp;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class UserManagementView implements Initializable{

	MainApp mainApp;
	LogView logView;

	@FXML
	GridPane gpUsers;

	/**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    @FXML
    public void goToHome(){
    	logView.subscribe();
    }

    @Override
	public void initialize(URL location, ResourceBundle resources) {
    	for(int i = 0; i < 7; i += 2) {
    		if(i != 0) {
    			gpUsers.addRow(i, null);
    		}
    		Image image = new Image("./pancake.png", 160, 160, true, false);
			ImageView imView = new ImageView(image);
    		gpUsers.add(imView, 0, 2*i, 1, 1);
    	}
    }
}
