package fr.esgi.ecoloresto.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Recipe;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;

public class WaitingRecipesView implements Initializable {
	MainApp mainApp;
	LogView logView;
	ArrayList<Recipe> alRecipes;

	@FXML
	GridPane gpRecipes;

	public static final String URL_SOURCE = "http://ecolorestoapi.herokuapp.com/recipes/Pierre";

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// TODO: Récupérer liste des plats en attente
		// TODO: Créer tableau de Recipes
		alRecipes = new ArrayList<>();
		alRecipes.add(new Recipe(5, "Name 0", "0 min preparation", "0 min baking", "0 person", "0 directions"));
		alRecipes.add(new Recipe(1, "Name 1", "1 min preparation", "1 min baking", "1 person", "1 directions"));
		alRecipes.add(new Recipe(2, "Name 2", "2 min preparation", "2 min baking", "2 person", "2 directions"));
		alRecipes.add(new Recipe(3, "Name 3", "3 min preparation", "3 min baking", "3 person", "3 directions"));
		alRecipes.add(new Recipe(4, "Name 4", "4 min preparation", "4 min baking", "4 person", "4 directions"));

		getDishes();

		fillGridPane();
	}


    void getDishes() {
    	/*** Create the request ***/
		// Create the URL:
		String query = URL_SOURCE;
		// Replace blanks with HTML-Equivalent:
		query = query.replace(" ", "%20");

		/***
		 * Make the request (This needs to be in a try-catch block because things can go wrong)
		 ***/
		try
		{
			// Turn the string into a URL object
			URL urlObject = new URL(query);
			// Open the stream (which returns an InputStream):
			InputStream in = urlObject.openStream();
			System.out.println(in);
			System.out.println("END");
			/** Now parse the data (the stream) that we received back ***/
			// Coming shortly since we need to set up a parser

			try (Scanner scanner = new Scanner(in)) {
			    String responseBody = scanner.useDelimiter("\\A").next();
			    System.out.println(responseBody);
			    //String weatherResult = ParseResult(responseBody);
			    //System.out.println(weatherResult);


			    JSONParser parser = new JSONParser();
			    try{
			    	Object obj = parser.parse(responseBody);

			    	//JSONObject jsonObject = (JSONObject) obj;

			    	//String nameOfCountry = (String) jsonObject.get("name");
			    	//System.out.println("Name Of Country: "+nameOfCountry);

			    	//long population = (Long) jsonObject.get("Population");
			    	//System.out.println("Population: "+population);

			    	//System.out.println("States are :");
			    	//JSONArray listOfStates = (JSONArray) jsonObject.get("States");
			    	//Iterator<String> iterator = listOfStates.iterator();
			    	//while (iterator.hasNext()) {
			    	//	System.out.println(iterator.next());
			    	//}

			    	JSONArray array = (JSONArray)obj;

			         System.out.println("The 2nd element of array");
			         System.out.println(array);
			         for(int i = 0; i < array.size(); i++) {
				         Object obj2 = parser.parse(array.get(i).toString());

				         JSONObject jsonObj = (JSONObject) obj2;

				         String name = (String) jsonObj.get("name");
				         System.out.println("<3 "+name);


				 		alRecipes.add(new Recipe(6, (String) jsonObj.get("name"), (String) jsonObj.get("preparation"), (String) jsonObj.get("baking"), (String) jsonObj.get("serving"), (String) jsonObj.get("directions")));
			         }
			         /*
			         System.out.println();
			         System.out.println(">>>>> "+"Field \"1\"");
			         System.out.println(">>>>> "+obj2.get("O"));

			         responseBody = "{}";
			         obj = parser.parse(responseBody);
			         System.out.println(">>>>> "+obj);

			         responseBody = "[5,]";
			         obj = parser.parse(responseBody);
			         System.out.println(">>>>> "+obj);

			         responseBody = "[5,,2]";
			         obj = parser.parse(responseBody);
			         System.out.println(">>>>> "+obj);
 */
			      }catch(ParseException pe) {
			         System.out.println("position: " + pe.getPosition());
			         System.out.println(pe);
			      }
			}

		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
    }

	/**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    public void fillGridPane() {
    	ObservableList<RowConstraints> contraints = gpRecipes.getRowConstraints();
		System.out.println(gpRecipes.getRowConstraints());
		gpRecipes.getChildren().clear();
		gpRecipes.getRowConstraints().clear();
		for(int i = 0 ; i < alRecipes.size() ; i++) {
			Text txt = new Text(alRecipes.get(i).getName());
			gpRecipes.add(txt, 1, i);
			Button ok = new Button("OK");
			ok.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					System.out.println("RECETTE ACCEPTEE");
				}
			});
			gpRecipes.add(ok, 2, i);
			Button nok = new Button("NOK");
			nok.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					System.out.println("RECETTE REFUSEE");
				}
			});
			gpRecipes.add(nok, 3, i);

			for(int j = 0; j < contraints.size(); j++)
				gpRecipes.getRowConstraints().add(contraints.get(j));
		}
    }
}
