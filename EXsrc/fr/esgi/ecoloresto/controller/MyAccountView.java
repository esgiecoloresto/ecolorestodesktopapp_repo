package fr.esgi.ecoloresto.controller;

import java.sql.SQLException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class MyAccountView {

	MainApp mainApp;
	LogView logView;
	Person person;

	@FXML
	ImageView ivAvatar;
	@FXML
	Text rating;
	@FXML
	TextField tfPseudo, tfEmail, tfNewPass, tfConfirmPass;
	@FXML
	ListView<String> lvLastTrades;

	@FXML
	public void initialize() throws SQLException {
		System.out.println(person.toString());

		rating.setText("4 / 5");
		tfPseudo.setText(person.getPseudo());
		tfEmail.setText(person.getEmail());

		ObservableList<String> olTrades = FXCollections.observableArrayList("Test 1", "Test 2", "Test 3", "Test 4", "Test 5", "Test 6", "Test 7", "Test 8", "Test 9", "Test 10");
		lvLastTrades.setItems(olTrades);
		lvLastTrades.setFixedCellSize(40.0);
	}

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }
    /**
    *
    */
   public void setPerson(Person person) {
   	this.person = person;
   }

    @FXML
    public void goToHome(){
    	logView.subscribe();
    }
}
