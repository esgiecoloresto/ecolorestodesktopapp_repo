package fr.esgi.ecoloresto.controller;

import java.io.IOException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Person;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class AccountView {

	MainApp mainApp;
	LogView logView;

	Person person;

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }
    /**
     *
     */
    public void setPerson(Person person) {
    	this.person = person;
    }

    public void goToAccount(){
    	try {
            // Chargement de LoginView.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/MyAccountView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Placement de LoginView dans le rootLayout.
            mainApp.rootLayout.setCenter(loginView);

         // Give the controller access to the main app.
            MyAccountView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
            controller.setPerson(person);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public void goToProposeMeal(){
		try {
            // Chargement de LoginView.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ProposeDishView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Placement de LoginView dans le rootLayout.
            mainApp.rootLayout.setCenter(loginView);

         // Give the controller access to the main app.
            ProposeDishView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void goToConsult(){
		try {
            // Chargement de LoginView.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ConsultView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Placement de LoginView dans le rootLayout.
            mainApp.rootLayout.setCenter(loginView);

         // Give the controller access to the main app.
            ConsultView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void goToProposeRecipe(){
		try {
            // Chargement de LoginView.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ProposeRecipeView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Placement de LoginView dans le rootLayout.
            mainApp.rootLayout.setCenter(loginView);

         // Give the controller access to the main app.
            ProposeRecipeView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

}
