package fr.esgi.ecoloresto.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class AddIngredientPopupController implements Initializable {
	
	@FXML
	private Button validBtn;
	@FXML
	private TextField name, function;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}
	
	public Button getValidBtn() {
		return validBtn;
	}
	
	public TextField getName() {
		return name;
	}
	
	public TextField getFunction() {
		return function;
	}
}