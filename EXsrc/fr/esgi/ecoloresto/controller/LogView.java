package fr.esgi.ecoloresto.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Person;
import fr.esgi.ecoloresto.model.Recipe;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class LogView implements Initializable {

	@FXML
	private TextField nicknameSubscribe, mailSubscribe, nicknameLogin;
	@FXML
	private PasswordField passSubscribe, confirmSubscribe, passLogin;
	@FXML
	private ImageView imgViewLogo;

	// Reference to the main application.
    private MainApp mainApp;

    private Person person;

    @FXML
    public void login() {
    	System.out.println(nicknameLogin.getText()+" - "+passLogin.getText());
    	StringBuffer sb = new StringBuffer();

    	try {
    		MessageDigest md = MessageDigest.getInstance("SHA-256");
    		String str = passLogin.getText();
    		byte[] h = md.digest(str.getBytes(StandardCharsets.UTF_8));
    		System.out.println(h.length+" ---- "+h.toString());
    		for(int i = 0; i< h.length; i++)
    			System.out.println(h[i]);

    		System.out.println("______________________");

    		for(int i = 0; i< h.length; i++)
    			sb.append(Integer.toString((h[i] & 0xff) +0x100,16).substring(1));

    		System.out.println(sb.toString());

    	} catch (NoSuchAlgorithmException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}

    	String url = "http://ecolorestoapi.herokuapp.com/user/";
    	String charset = "UTF-8";
    	String query = String.format("");

    	URLConnection connection = null;
    	InputStream response = null;
    	try {
    		connection = new URL(url).openConnection();

    		//connection.setRequestProperty("Accept-Charset", charset);
    		connection.setRequestProperty("Pseudo", nicknameLogin.getText());
    		connection.setRequestProperty("Password", sb.toString());
    		System.setProperty("http.keepAlive", "false");

    		response = connection.getInputStream();
    	} catch (IOException e1) {
    		// TODO Auto-generated catch block
    		e1.printStackTrace();
    	}

    	try (Scanner scanner = new Scanner(response)) {
    		String responseBody = scanner.useDelimiter("\\A").next();
    		System.out.println(responseBody);


    		JSONParser parser = new JSONParser();
    		try{
    			Object obj = parser.parse(responseBody);
    			JSONObject jsonObj = (JSONObject) obj;

    			System.out.println("The 2nd element of array");
    			person = new Person((long) jsonObj.get("idPERSON"), (String) jsonObj.get("pseudo"), (String) jsonObj.get("email"), 4);
    			/*		         for(int i = 0; i < array.size(); i++) {
			         Object obj2 = parser.parse(array.get(i).toString());

			         JSONObject jsonObj = (JSONObject) obj2;

			         String name = (String) jsonObj.get("name");
			         System.out.println("<3 "+name);


			 		alRecipes.add(new Recipe(6, (String) jsonObj.get("name"), (String) jsonObj.get("preparation"), (String) jsonObj.get("baking"), (String) jsonObj.get("serving"), (String) jsonObj.get("directions")));
		         }
    			 */
    		}catch(ParseException pe) {
    			System.out.println("position: " + pe.getPosition());
    			System.out.println(pe);
    		}
    	}
    	try {
            // Chargement de LoginView.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/AccountView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Placement de LoginView dans le rootLayout.
            mainApp.rootLayout.setCenter(loginView);

            // Donne les accès à MainApp au controller
            AccountView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(this);
            controller.setPerson(person);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void subscribe() {



    	// Vérifier le compte
    	System.out.println(nicknameSubscribe.getText()+" - "+passSubscribe.getText()+" - "+confirmSubscribe.getText()+" - "+mailSubscribe.getText());
    	//showLoginView("AccountView.fxml");

    	if(nicknameSubscribe.getText().equals("admin")) {
    		try {
	            // Chargement de LoginView.
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(MainApp.class.getResource("view/AdminAccountView.fxml"));
	            AnchorPane loginView = (AnchorPane) loader.load();

	            // Placement de LoginView dans le rootLayout.
	            mainApp.rootLayout.setCenter(loginView);

	            // Donne les accès à MainApp au controller
	            AdminAccountView controller = loader.getController();
	            controller.setMainApp(mainApp);
	            controller.setLogView(this);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
    	} else {
	    	try {
	            // Chargement de LoginView.
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(MainApp.class.getResource("view/AccountView.fxml"));
	            AnchorPane loginView = (AnchorPane) loader.load();

	            // Placement de LoginView dans le rootLayout.
	            mainApp.rootLayout.setCenter(loginView);

	            // Donne les accès à MainApp au controller
	            AccountView controller = loader.getController();
	            controller.setMainApp(mainApp);
	            controller.setLogView(this);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
    	}

    	//mainApp.showLoginView("AccountView.fxml");
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("TEST");
	}
}
