package fr.esgi.ecoloresto.controller;

import java.net.URL;
import java.util.ResourceBundle;

import fr.esgi.ecoloresto.MainApp;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;

public class ConsultView implements Initializable{
	MainApp mainApp;
	LogView logView;

	@FXML
	private ImageView ivRecipe;
	@FXML
	private GridPane gpRecipes;
	@FXML
	private Text tTitle, tDescr;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
//		System.out.println("TEST");
//		File file = new File("./pancake.jpg");
//		Image image = new Image(file.toURI().toString());
//		ivRecipe.setImage(image);
//		ivRecipe.setStyle("-fx-background-color: BLACK;");

/*		int row1 = 2, row2 = 2;
		for(int i = 0 ; i < 3 ; i++) {
			row1 += i*2;
			row2 += i*2+1;
			ImageView imageView = new ImageView();
			imageView.setStyle(ivRecipe.getStyle());
			Text txt1 = new Text("AAA");
			txt1.setStyle(tTitle.getStyle());
			Text txt2 = new Text("BBB");
			txt2.setStyle(tDescr.getStyle());
			gpRecipes.addRow(row1, imageView, txt1);
			gpRecipes.addRow(row2, txt2);
			gpRecipes.getRow
		}
*/
		// Add 250 rows to test performance:
	      addNewRow(gpRecipes, 1);
	}
	private void addNewRow(GridPane gridPane, int rowIndex) {
	    /*
	    int numRows = 1 ;
	    for (Node node : gpRecipes.getChildren()) {
	    	System.out.println("TEST ! "+node.toString());
	    	int currentRow = gpRecipes.getR
	    	if (currentRow >= rowIndex) {
	    		GridPane.setRowIndex(node, currentRow+1);
	    		if (currentRow+1 > numRows) {
	    			numRows = currentRow + 1;
	    		}
	    	}
	    	System.out.println("END !");
	    }
	    String color = numRows % 2 == 0 ? "lightskyblue" : "cornflowerblue" ;
	    for (int i=0; i<2; i++) {
	      Label label = new Label(String.format("Label [%d, %d]", numRows, i+1));
	      label.setStyle("-fx-background-color: "+color);
	      label.setMaxWidth(Double.POSITIVE_INFINITY);
	      GridPane.setHgrow(label, Priority.ALWAYS);
	      gridPane.add(label, i, rowIndex);
	    }
	    */

		ObservableList<RowConstraints> contraints = gpRecipes.getRowConstraints();
		System.out.println(gpRecipes.getRowConstraints());
		gpRecipes.getChildren().clear();
		gpRecipes.getRowConstraints().clear();
		for(int i = 0 ; i < 10 ; i += 2) {
			Image image = new Image("../view/assets/pancake.png", 160, 160, true, false);
			ImageView imView = new ImageView(image);
			imView.setStyle("-fx-background-color:transparent;");
			imView.autosize();
			gpRecipes.add(imView, 0, i, 1, 2);
//			gpRecipes.setRowSpan(imView, 1);
			Text txt1 = new Text("Nom de la recette : ");
			Text txt2 = new Text("Ingrédients : ");
			gpRecipes.add(txt1, 1, i);
			gpRecipes.add(txt2, 1, i+1);
			if(i == 0) {
			}

			for(int j = 0; j < contraints.size(); j++)
				gpRecipes.getRowConstraints().add(contraints.get(j));
		}
	  }


    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    @FXML
    public void goToHome(){
    	logView.subscribe();
    }

}
