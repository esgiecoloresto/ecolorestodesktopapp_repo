package fr.esgi.ecoloresto.controller;

import java.io.IOException;

import fr.esgi.ecoloresto.MainApp;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ProposeRecipeView{
    MainApp mainApp;
	LogView logView;

	@FXML
	private TextField tfRecipeName;
	@FXML
	private TextArea taRecipeIngredient, taRecipeDetails;

	/**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    @FXML
    public void sendRecipe(){
    	System.out.println("Nom: "+tfRecipeName.getText()+".");
    	System.out.println("Ingrédients: "+taRecipeIngredient.getText()+".");
    	System.out.println("Détails: "+taRecipeDetails.getText()+".");
    }

    @FXML
    public void addIngredient() throws IOException{
		// Instantiation de la popup
		Stage popupStage = new Stage(StageStyle.UNIFIED);
		FXMLLoader loader = new FXMLLoader();
		AnchorPane popupWdw = (AnchorPane) loader.load(AddIngredientPopupController.class.getResourceAsStream("/fr/esgi/ecoloresto/view/AddIngredientView.fxml"));
		Scene popupScene = new Scene(popupWdw);
		popupStage.setScene(popupScene);
		popupStage.initOwner(MainApp.getPrimaryStage());
		popupStage.setTitle("Ajout d'un ingrédient");

		// Controller de la popup
		AddIngredientPopupController controller = loader.getController();

		EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				/* TODO */
				popupStage.close();
			}
		};
		controller.getValidBtn().setOnAction(eventHandler);
		popupStage.show();
    }

    @FXML
    public void goToHome(){
    	logView.subscribe();
    }
}
