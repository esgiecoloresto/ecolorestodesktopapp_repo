package fr.esgi.ecoloresto.model;

public class Person {

	private long id;
	private String pseudo;
	private String email;
	private int rating;

	public Person(long id, String email) {
		super();
		this.id = id;
		this.email = email;
	}
	public Person(long id, String pseudo, String email,  int rating) {
		super();
		this.id = id;
		this.pseudo = pseudo;
		this.email = email;
		this.rating = rating;
	}

	public String toString() {
		return this.getPseudo()+" "+this.getEmail();
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
}
