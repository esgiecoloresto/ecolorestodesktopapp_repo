package fr.esgi.ecoloresto.model;

public class Counterparty {

	private int id;
	private String name;
	private Double priceIfPecuniary;
	private Boolean isFavoriteCounterparty;



	public Counterparty(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Counterparty(int id, String name, Double priceIfPecuniary, Boolean isFavoriteCounterparty) {
		super();
		this.id = id;
		this.name = name;
		this.priceIfPecuniary = priceIfPecuniary;
		this.isFavoriteCounterparty = isFavoriteCounterparty;
	}
	/*
	 * GETTERS AND SETTERS
	 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPriceIfPecuniary() {
		return priceIfPecuniary;
	}
	public void setPriceIfPecuniary(Double priceIfPecuniary) {
		this.priceIfPecuniary = priceIfPecuniary;
	}
	public Boolean getIsFavoriteCounterparty() {
		return isFavoriteCounterparty;
	}
	public void setIsFavoriteCounterparty(Boolean isFavoriteCounterparty) {
		this.isFavoriteCounterparty = isFavoriteCounterparty;
	}

}
