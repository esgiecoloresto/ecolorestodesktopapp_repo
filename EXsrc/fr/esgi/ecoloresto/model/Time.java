package fr.esgi.ecoloresto.model;

import java.util.Date;

public class Time {

	private int id;
	private Date date;
	private java.sql.Time time;
	private Boolean isDepositDate;
	private Boolean isEndDate;

	public Time(int id, Date date, java.sql.Time time, Boolean isDepositDate, Boolean isEndDate) {
		super();
		this.id = id;
		this.date = date;
		this.time = time;
		this.isDepositDate = isDepositDate;
		this.isEndDate = isEndDate;
	}
	/*
	 * GETTERS AND SETTERS
	 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public java.sql.Time getTime() {
		return time;
	}
	public void setTime(java.sql.Time time) {
		this.time = time;
	}
	public Boolean getIsDepositDate() {
		return isDepositDate;
	}
	public void setIsDepositDate(Boolean isDepositDate) {
		this.isDepositDate = isDepositDate;
	}
	public Boolean getIsEndDate() {
		return isEndDate;
	}
	public void setIsEndDate(Boolean isEndDate) {
		this.isEndDate = isEndDate;
	}
}
