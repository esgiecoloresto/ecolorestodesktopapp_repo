package fr.esgi.ecoloresto.model;

import java.util.ArrayList;

public class Dish {

	private int id;
	private String name;
	private String dishDescription;
	private String advices;
	private Boolean isReserved;
	private Boolean isInProgress;

	private ArrayList<Address> addressList;


	public Dish(int id, String name, String dishDescription) {
		super();
		this.id = id;
		this.name = name;
		this.dishDescription = dishDescription;
	}
	public Dish(int id, String name, String dishDescription, String advices, Boolean isReserved, Boolean isInProgress,
			ArrayList<Address> addressList) {
		super();
		this.id = id;
		this.name = name;
		this.dishDescription = dishDescription;
		this.advices = advices;
		this.isReserved = isReserved;
		this.isInProgress = isInProgress;
		this.addressList = addressList;
	}


	/**
	 * GETTERS AND SETTERS
	 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDishDescription() {
		return dishDescription;
	}
	public void setDishDescription(String dishDescription) {
		this.dishDescription = dishDescription;
	}
	public String getAdvices() {
		return advices;
	}
	public void setAdvices(String advices) {
		this.advices = advices;
	}
	public Boolean getIsReserved() {
		return isReserved;
	}
	public void setIsReserved(Boolean isReserved) {
		this.isReserved = isReserved;
	}
	public Boolean getIsInProgress() {
		return isInProgress;
	}
	public void setIsInProgress(Boolean isInProgress) {
		this.isInProgress = isInProgress;
	}
	public ArrayList<Address> getAddressList() {
		return addressList;
	}
	public void setAddressList(ArrayList<Address> addressList) {
		this.addressList = addressList;
	}
}
