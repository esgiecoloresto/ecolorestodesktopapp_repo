package fr.esgi.ecoloresto.model;

public class Address {

	private long id;
	private String firstName;
	private String lastName;
	private String address;
	private String addressComplement1, addressComplement2;
	private String zipCode;
	private String city;




	public Address(long id, String lastName, String address, String zipCode, String city) {
		super();
		this.id = id;
		this.lastName = lastName;
		this.address = address;
		this.zipCode = zipCode;
		this.city = city;
	}

	public Address(long id, String firstName, String lastName, String address, String addressComplement1,
			String addressComplement2, String zipCode, String city) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.addressComplement1 = (addressComplement1.equals(""))?null:addressComplement1;
		this.addressComplement2 = (addressComplement2.equals(""))?null:addressComplement2;
		this.zipCode = zipCode;
		this.city = city;
	}
	/**
	 * GETTERS AND SETTERS
	 */
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressComplement1() {
		return addressComplement1;
	}
	public void setAddressComplement1(String addressComplement1) {
		this.addressComplement1 = addressComplement1;
	}
	public String getAddressComplement2() {
		return addressComplement2;
	}
	public void setAddressComplement2(String addressComplement2) {
		this.addressComplement2 = addressComplement2;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
}
