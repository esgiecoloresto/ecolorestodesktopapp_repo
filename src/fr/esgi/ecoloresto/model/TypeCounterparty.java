package fr.esgi.ecoloresto.model;

public class TypeCounterparty {

	private long id;
	private String name;

	public TypeCounterparty(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * GETTERS & SETTERS
	 */
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
