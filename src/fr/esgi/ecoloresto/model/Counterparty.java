package fr.esgi.ecoloresto.model;

public class Counterparty {

	private long id;
	private String name;
	private double priceIfPecuniary;
	private int idType;


	public Counterparty(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Counterparty(long id, String name, double priceIfPecuniary) {
		super();
		this.id = id;
		this.name = name;
		this.priceIfPecuniary = priceIfPecuniary;
	}

	/*
	 * GETTERS AND SETTERS
	 */
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPriceIfPecuniary() {
		return priceIfPecuniary;
	}
	public void setPriceIfPecuniary(double priceIfPecuniary) {
		this.priceIfPecuniary = priceIfPecuniary;
	}
	public int getIdType() {
		return idType;
	}
	public void setIdType(int idType) {
		this.idType = idType;
	}

}
