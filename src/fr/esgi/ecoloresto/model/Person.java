package fr.esgi.ecoloresto.model;

public class Person {

	private long id;
	private String pseudo;
	private String email;
	private int rating;
	private String url_image;
	private long id_image;
	private boolean isAdmin;


	private Dish[] dishes;

	public Person(String pseudo) {
		super();
		this.pseudo = pseudo;
	}

	public Person(long id, String email) {
		super();
		this.id = id;
		this.email = email;
	}
	public Person(long id, String pseudo, String email,  int rating, String url_image) {
		super();
		this.id = id;
		this.pseudo = pseudo;
		this.email = email;
		this.rating = rating;
		this.url_image = url_image;
	}

	public Person(long id, String pseudo, String email,  int rating, long id_image) {
		super();
		this.id = id;
		this.pseudo = pseudo;
		this.email = email;
		this.rating = rating;
		this.id_image = id_image;
	}

	public String toString() {
		return this.getPseudo()+" "+this.getEmail();
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public String getUrl_image() {
		return url_image;
	}
	public void setUrl_image(String url_image) {
		this.url_image = url_image;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public Dish[] getDishes() {
		return dishes;
	}
	public void setDishes(Dish[] dishes) {
		this.dishes = dishes;
	}
	public long getId_image() {
		return id_image;
	}

	public void setId_image(long id_image) {
		this.id_image = id_image;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
}
