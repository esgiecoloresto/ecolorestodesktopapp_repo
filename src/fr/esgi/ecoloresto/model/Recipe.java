package fr.esgi.ecoloresto.model;

public class Recipe {

	private int id;
	private String name;
	private String preparation;
	private String baking;
	private String serving;
	private String directions;
	private String url_image;
	private Person person;



	public Recipe(int id, String name, String preparation, String baking, String serving, String directions,
			String url_image) {
		super();
		this.id = id;
		this.name = name;
		this.preparation = preparation;
		this.baking = baking;
		this.serving = serving;
		this.directions = directions;
		this.url_image = url_image;
	}

	private Ingredient[] ingredients;



	public Recipe(int id, String name, String preparation) {
		super();
		this.id = id;
		this.name = name;
		this.preparation = preparation;
	}

	public Recipe(int id, String name, String preparation, String baking, String serving, String directions) {
		super();
		this.id = id;
		this.name = name;
		this.preparation = preparation;
		this.baking = baking;
		this.serving = serving;
		this.directions = directions;
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPreparation() {
		return preparation;
	}
	public void setPreparation(String preparation) {
		this.preparation = preparation;
	}
	public String getBaking() {
		return baking;
	}
	public void setBaking(String baking) {
		this.baking = baking;
	}
	public String getServing() {
		return serving;
	}
	public void setServing(String serving) {
		this.serving = serving;
	}
	public String getDirections() {
		return directions;
	}
	public void setDirections(String directions) {
		this.directions = directions;
	}
	public Ingredient[] getIngredients() {
		return ingredients;
	}

	public void setIngredients(Ingredient[] ingredients) {
		this.ingredients = ingredients;
	}
	public String getUrl_image() {
		return url_image;
	}

	public void setUrl_image(String url_image) {
		this.url_image = url_image;
	}
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
}
