package fr.esgi.ecoloresto.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Ingredient;
import fr.esgi.ecoloresto.model.Recipe;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class ConsultView implements Initializable{
	MainApp mainApp;
	LogView logView;

	@FXML
	private ImageView ivRecipe;
	@FXML
	private GridPane gpRecipes;
	@FXML
	private Text tTitle, tDescr;


	@SuppressWarnings("static-access")
	@Override
	public void initialize(URL location, ResourceBundle resources) {


    	String url = "http://ecolorestoapi.herokuapp.com/recipes/";

    	URLConnection connection = null;
    	InputStream response = null;
    	try {
    		connection = new URL(url).openConnection();
    		response = connection.getInputStream();
    	} catch (IOException e1) {
    		// TODO Auto-generated catch block
    		e1.printStackTrace();
    	}

    	try (Scanner scanner = new Scanner(response)) {
    		String responseBody = scanner.useDelimiter("\\A").next();
    		System.out.println("#####" + responseBody);


    		JSONParser parser = new JSONParser();
    		try{
    			Object obj = parser.parse(responseBody);
    			JSONArray jsonRecipes = (JSONArray) obj;

    			ObservableList<RowConstraints> contraints;
    			contraints = gpRecipes.getRowConstraints();
    			System.out.println(gpRecipes.getRowConstraints());
    			System.out.println("1********** "+contraints);
    			gpRecipes.getChildren().clear();
    			System.out.println("2********** "+contraints);
    			gpRecipes.getRowConstraints().clear();
    			System.out.println("3********** "+contraints);
    			Recipe[] recipes = new Recipe[jsonRecipes.size()];
    			for(int i = 0; i < jsonRecipes.size(); i++) {
        			System.out.println("4********** "+contraints);
    				Object objDish = parser.parse(jsonRecipes.get(i).toString());
    				JSONObject jsonRecipe = (JSONObject) objDish;
    				if((boolean) jsonRecipe.get("isAccepted")) {

	    				JSONArray jsonIngredients = (JSONArray) jsonRecipe.get("ingredients");
	    				Ingredient[] ingredients = new Ingredient[jsonIngredients.size()];

	    				for(int j = 0; j < jsonIngredients.size(); j++) {
	        				Object objIngredient = parser.parse(jsonIngredients.get(j).toString());
	        				JSONObject jsonIngredient = (JSONObject) objIngredient;
	        				System.out.println(">>>>>>>>> "+(String) jsonIngredient.get("name"));
	        				ingredients[j] = new Ingredient(j, (String) jsonIngredient.get("name"));
	        			}

	    				System.out.println(">>>> "+(String) jsonRecipe.get("name"));
	    				recipes[i] = new Recipe(i, (String) jsonRecipe.get("name"), (String) jsonRecipe.get("preparation"), (String) jsonRecipe.get("baking"), (String) jsonRecipe.get("serving"), (String) jsonRecipe.get("directions"));
	    				if(ingredients != null)
	    					recipes[i].setIngredients(ingredients);;

	    				Text txt1 = new Text("Nom de la recette : " + recipes[i].getName());
	    				String sIngredients = "";

	    				sIngredients += "\n" + recipes[i].getServing() + "\nPréparation : " + recipes[i].getPreparation() + "\nCuisson : " + recipes[i].getPreparation() + "\n\n";
	    				sIngredients += "Ingrédients : ";
	    				for(int k = 0; k < recipes[i].getIngredients().length; k++) {
	    					sIngredients += recipes[i].getIngredients()[k].getName();
	    					if(k != recipes[i].getIngredients().length-1)
	    						sIngredients += ", ";
	    				}
	    				sIngredients += "\n\n" + recipes[i].getDirections();

	    				Text txt2 = new Text(sIngredients);
	    				TextFlow tflow = new TextFlow(txt2);
	    				gpRecipes.add(txt1, 1, i*3);
	    				gpRecipes.add(tflow, 1, i*3+1);

	    				JSONArray jsonImages = (JSONArray) jsonRecipe.get("images");
	    				String imgLink = "";

	    				for(int l = 0; l < jsonImages.size(); l++) {
	    					JSONObject image = (JSONObject) parser.parse(jsonImages.get(l).toString());
	    					if((boolean) image.get("isPrincipalImage")) {
	    						imgLink = image.get("link").toString();
	    					}
	    				}
	    				if(!imgLink.equals("")) {
		    		        Image image = new Image(imgLink, 160.0, 160.0, true, true);
		    		        ImageView imageView = new ImageView(image);
		    				gpRecipes.add(imageView, 0, i*3, 1, 2);
		    				gpRecipes.setValignment(imageView, VPos.TOP);
	    				}
	    				Separator sep = new Separator(Orientation.HORIZONTAL);
	    				sep.setPrefSize(250, 10);
	    				sep.setHalignment(HPos.CENTER);

	    				gpRecipes.add(sep, 0, i*3+2, 2, 1);
    				}
    			}
    		}catch(ParseException pe) {
    			System.out.println("position: " + pe.getPosition());
    			System.out.println(pe);
    		}
    	}

	}
//	private void addNewRow(GridPane gridPane, int rowIndex) {
//	    /*
//	    int numRows = 1 ;
//	    for (Node node : gpRecipes.getChildren()) {
//	    	System.out.println("TEST ! "+node.toString());
//	    	int currentRow = gpRecipes.getR
//	    	if (currentRow >= rowIndex) {
//	    		GridPane.setRowIndex(node, currentRow+1);
//	    		if (currentRow+1 > numRows) {
//	    			numRows = currentRow + 1;
//	    		}
//	    	}
//	    	System.out.println("END !");
//	    }
//	    String color = numRows % 2 == 0 ? "lightskyblue" : "cornflowerblue" ;
//	    for (int i=0; i<2; i++) {
//	      Label label = new Label(String.format("Label [%d, %d]", numRows, i+1));
//	      label.setStyle("-fx-background-color: "+color);
//	      label.setMaxWidth(Double.POSITIVE_INFINITY);
//	      GridPane.setHgrow(label, Priority.ALWAYS);
//	      gridPane.add(label, i, rowIndex);
//	    }
//	    */
//
//		ObservableList<RowConstraints> contraints = gpRecipes.getRowConstraints();
//		System.out.println(gpRecipes.getRowConstraints());
//		gpRecipes.getChildren().clear();
//		gpRecipes.getRowConstraints().clear();
//		for(int i = 0 ; i < 10 ; i += 2) {
////			Image image = new Image("../view/assets/pancake.png", 160, 160, true, false);
////			ImageView imView = new ImageView(image);
////			imView.setStyle("-fx-background-color:transparent;");
////			imView.autosize();
////			gpRecipes.add(imView, 0, i, 1, 2);
////			gpRecipes.setRowSpan(imView, 1);
//			Text txt1 = new Text("Nom de la recette : ");
//			Text txt2 = new Text("Ingrédients : ");
//			gpRecipes.add(txt1, 1, i);
//			gpRecipes.add(txt2, 1, i+1);
//			if(i == 0) {
//			}
//
//			for(int j = 0; j < contraints.size(); j++)
//				gpRecipes.getRowConstraints().add(contraints.get(j));
//		}
//	  }


    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    @FXML
    public void goToHome(){
    	logView.loadAccount();
    }

}
