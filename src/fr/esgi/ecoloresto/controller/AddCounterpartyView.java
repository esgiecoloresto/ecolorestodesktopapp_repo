package fr.esgi.ecoloresto.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

public class AddCounterpartyView implements Initializable {

	@FXML
	private ChoiceBox<String> typeCounterparty, counterparty;
	@FXML
	private TextField tfValue;
	@FXML
	private Button send;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	/**
	 * GETTERS & SETTERS
	 */
	public ChoiceBox<String> getTypeCounterparty() {
		return typeCounterparty;
	}

	public void setTypeCounterparty(ChoiceBox<String> typeCounterparty) {
		this.typeCounterparty = typeCounterparty;
	}

	public ChoiceBox<String> getCounterparty() {
		return counterparty;
	}

	public void setCounterparty(ChoiceBox<String> counterparty) {
		this.counterparty = counterparty;
	}

	public TextField getTfValue() {
		return tfValue;
	}

	public void setTfValue(TextField tfValue) {
		this.tfValue = tfValue;
	}

	public Button getSend() {
		return send;
	}

	public void setSend(Button send) {
		this.send = send;
	}

}
