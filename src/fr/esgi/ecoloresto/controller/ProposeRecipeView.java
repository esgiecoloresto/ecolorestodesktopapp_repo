package fr.esgi.ecoloresto.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Ingredient;
import fr.esgi.ecoloresto.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ProposeRecipeView implements Initializable{
    MainApp mainApp;
	LogView logView;
	int nbImg;
	String url1, url2, url3;
	private Person person;

	ObservableList<Ingredient> olIngredients = FXCollections.observableArrayList();
	ObservableList<String> olIngStr = FXCollections.observableArrayList();

	@FXML
	private TextField tfRecipeName, tfBaking, tfServing, tfPreparation;
	@FXML
	private TextArea taRecipeDetails;
	@FXML
	private ListView<String> lvIngs;
	@FXML
	private VBox vbox1, vbox2, vbox3;
	@FXML
	Button addImg;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		lvIngs.setItems(olIngStr);
		person = (Person) ContextAccount.getInstance().getContextObject("myPerson");
		nbImg = 0;
		addImg.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				Button button = (Button) event.getSource();
				if(button.getText().equals("Ajouter")) {
					switch (nbImg) {
					case 0:
						vbox1.setDisable(false);
						TextField tf1 = new TextField("http://");
						vbox1.getChildren().add(tf1);
						break;
					case 1:
						vbox2.setDisable(false);
						TextField tf2 = new TextField("http://");
						vbox2.getChildren().add(tf2);
						break;
					case 2:
						vbox3.setDisable(false);
						TextField tf3 = new TextField("http://");
						vbox3.getChildren().add(tf3);
						break;
					default:
						break;
					}
					addImg.setText("Valider");
				} else {
					switch(nbImg) {

					case 2 :
						if(vbox3.getChildren().size() == 1) {
							TextField tf3 = (TextField) vbox3.getChildren().get(0);
							url3 = tf3.getText();
							Image image3 = new Image(url3, 120.0, 120.0, true, true);
							System.out.println(image3.toString());
		    		        ImageView imageView3 = new ImageView(image3);
		    		        vbox3.getChildren().add(0, imageView3);
		    		        addImg.setText("Modifier");
						} else {
							TextField tf3 = (TextField) vbox3.getChildren().get(1);
							ImageView imageView3 = (ImageView) vbox3.getChildren().get(0);
							if(!tf3.getText().equals(url3)) {
								Image image3 = new Image(tf3.getText(), 120.0, 120.0, true, true);
								imageView3.setImage(image3);
							}
						}

					case 1 :
						if(vbox2.getChildren().size() == 1) {
							TextField tf2 = (TextField) vbox2.getChildren().get(0);
							url2 = tf2.getText();
							Image image2 = new Image(url2, 120.0, 120.0, true, true);
							System.out.println(image2.toString());
		    		        ImageView imageView2 = new ImageView(image2);
		    		        vbox2.getChildren().add(0, imageView2);
		    		        addImg.setText("Ajouter");
		    		        nbImg++;
						} else {
							TextField tf2 = (TextField) vbox2.getChildren().get(1);
							ImageView imageView2 = (ImageView) vbox2.getChildren().get(0);
							if(!tf2.getText().equals(url2)) {
								Image image2 = new Image(tf2.getText(), 120.0, 120.0, true, true);
								imageView2.setImage(image2);
							}
						}

					case 0:
						if(vbox1.getChildren().size() == 1) {
							TextField tf1 = (TextField) vbox1.getChildren().get(0);
							url1 = tf1.getText();
							Image image1 = new Image(url1, 120.0, 120.0, true, true);
							System.out.println(image1.toString());
		    		        ImageView imageView1 = new ImageView(image1);
		    		        vbox1.getChildren().add(0, imageView1);
		    		        addImg.setText("Ajouter");
		    		        nbImg++;
						} else {
							TextField tf1 = (TextField) vbox1.getChildren().get(1);
							ImageView imageView1 = (ImageView) vbox1.getChildren().get(0);
							if(!tf1.getText().equals(url1)) {
								Image image1 = new Image(tf1.getText(), 120.0, 120.0, true, true);
								imageView1.setImage(image1);
							}
						}
	    		        break;
					}
				}
			}
		});
	}


	/**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    @SuppressWarnings("unchecked")
	@FXML
    public void sendRecipe(){

    	String missingArgs = "";
		if(tfRecipeName.getText().equals(""))
			missingArgs += "\nNom du plat manquant";
		if(vbox1.getChildren().size() != 2)
			missingArgs += "\nVeuillez mettre au moins 1 image";
		if(tfServing.getText().equals(""))
			missingArgs += "\nNombre de parts manquant";
		if(tfPreparation.getText().equals(""))
			missingArgs += "\nTemps de préparation manquant";
		if(tfBaking.getText().equals(""))
			missingArgs += "\nTemps de cuisson manquant";
		if(olIngStr.size() == 0)
			missingArgs += "\nVeuillez mettre au moins 1 ingrédient";
		if(taRecipeDetails.getText().equals(""))
			missingArgs += "\nVeuillez décrire votre recette";
		if(!missingArgs.equals("")) {
			// Alert if Login or Password are empty
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Veuillez remplir les champs suivants :" + missingArgs);
			alert.showAndWait();
		} else {

	    	// Prepare JSONObject
	    	JSONObject jsonRecipe = new JSONObject();
	    	jsonRecipe.put("name", tfRecipeName.getText());
	    	jsonRecipe.put("preparation", tfPreparation.getText());
	    	jsonRecipe.put("baking", tfBaking.getText());
	    	jsonRecipe.put("serving", tfServing.getText());
	    	jsonRecipe.put("directions", taRecipeDetails.getText());
	    	jsonRecipe.put("pseudo", person.getPseudo());

	    	JSONArray jsonIngs = new JSONArray();
	    	for(int i = 0; i < olIngredients.size(); i++) {
	    		jsonIngs.add(olIngredients.get(i).getName());
	    	}
	    	jsonRecipe.put("ingredients", jsonIngs);


	    	// Prepare request
	    	String http = "http://ecolorestoapi.herokuapp.com/recipe";
			HttpURLConnection urlConnection=null;
		    URL url;
			try {
				url = new URL(http);
				byte[] postDataBytes = jsonRecipe.toString().getBytes("UTF-8");
				urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setRequestMethod("POST");
				urlConnection.setRequestProperty("Content-Type","application/json");
				urlConnection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
				urlConnection.setDoOutput(true);
				urlConnection.getOutputStream().write(postDataBytes);

	//		    urlConnection.connect();


	//		    printout = new DataOutputStream(urlConnection.getOutputStream ());
	//			printout.write(postDataBytes);
	//			printout.flush ();
	//			printout.close ();
	//			System.out.println(urlConnection.getResponseCode() + " - "  + urlConnection.getResponseMessage());

				//input = urlConnection.getInputStream();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Reader in;
			try {
				in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = in.read()) >= 0;)
					sb.append((char)c);
				String response = sb.toString();
				System.out.println("##### = "+response);

				JSONParser parser = new JSONParser();

				Object obj = parser.parse(response);
				JSONObject jsonObj = (JSONObject) obj;
				long idRECIPE = (long) jsonObj.get("idRECIPE");

				if(vbox1.getChildren().size() == 2) {
					TextField tf = (TextField) vbox1.getChildren().get(1);
					sendImgForRecipe(idRECIPE, tf.getText(), 1);
				}
				if(vbox2.getChildren().size() == 2) {
					TextField tf = (TextField) vbox2.getChildren().get(1);
					sendImgForRecipe(idRECIPE, tf.getText(), 2);
				}
				if(vbox3.getChildren().size() == 2) {
					TextField tf = (TextField) vbox3.getChildren().get(1);
					sendImgForRecipe(idRECIPE, tf.getText(), 3);
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Alert if Login or Password are empty
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Votre recette a bien été envoyée.");
			alert.showAndWait();

			goToHome();
		}
    }


    private void sendImgForRecipe(long idRecipe, String link, int i) {

		URL url;
		try {
			url = new URL("http://ecolorestoapi.herokuapp.com/image");
			Map<String,Object> params = new LinkedHashMap<>();
			params.put("name", "imageRecipe1");
			params.put("link", link);
			params.put("isPrincipalImage", (i==1)?true:false);
			params.put("RECIPE_id", idRecipe);

			StringBuilder postData = new StringBuilder();
			for (Map.Entry<String,Object> param : params.entrySet()) {
				if (postData.length() != 0) postData.append('&');
				postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
				postData.append('=');
				postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			}
			byte[] postDataBytes = postData.toString().getBytes("UTF-8");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
			conn.setDoOutput(true);
			conn.getOutputStream().write(postDataBytes);

			System.out.println(conn.getResponseCode() + " : " + conn.getResponseMessage());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    @FXML
    public void addIngredient() throws IOException{
		// Instantiation de la popup
		Stage popupStage = new Stage(StageStyle.UNIFIED);
		FXMLLoader loader = new FXMLLoader();
		AnchorPane popupWdw = (AnchorPane) loader.load(AddIngredientPopupController.class.getResourceAsStream("/fr/esgi/ecoloresto/view/AddIngredientView.fxml"));
		Scene popupScene = new Scene(popupWdw);
		popupStage.setScene(popupScene);
		popupStage.setResizable(false);
		popupStage.initOwner(MainApp.getPrimaryStage());
		popupStage.setTitle("Ajout d'un ingrédient");

		// Controller de la popup
		AddIngredientPopupController controller = loader.getController();

		EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				/* TODO */
				olIngStr.add(controller.getName().getText());
				olIngredients.add(new Ingredient(0, controller.getName().getText()));
				popupStage.close();
			}
		};
		controller.getValidBtn().setOnAction(eventHandler);
		popupStage.show();
    }

    @FXML
	public void goToHome(){
		logView.loadAccount();
	}
}
