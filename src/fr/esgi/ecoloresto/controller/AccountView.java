package fr.esgi.ecoloresto.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Person;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

public class AccountView implements Initializable{

	MainApp mainApp;
	LogView logView;

	Person person;


    public void goToAccount(){
    	try {
            // Load user account view.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/MyAccountView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Set user account view to rootLayout.
            mainApp.rootLayout.setCenter(loginView);

         // Give the controller access to the main app.
            MyAccountView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public void goToProposeMeal(){
		try {
            // Load ProposeMeal view.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ProposeDishView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Set ProposeMeal to rootLayout.
            mainApp.rootLayout.setCenter(loginView);

            // Give the controller access to the main app.
            ProposeDishView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void goToConsult(){
		try {
            // Load Consult view.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ConsultView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Set Consult view to rootLayout.
            mainApp.rootLayout.setCenter(loginView);

            // Give the controller access to the main app.
            ConsultView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void goToProposeRecipe(){
		try {
            // Load ProposeRecipeView.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ProposeRecipeView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // set ProposeRecipe view to rootLayout.
            mainApp.rootLayout.setCenter(loginView);

            // Give the controller access to the main app.
            ProposeRecipeView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}
	/**
	 * Is called by the main application to give a reference back to itself.
	 *
	 * @param mainApp
	 */
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}
	/**
	 * Is called by the main application to give a reference back to itself.
	 *
	 * @param mainApp
	 */
	public void setLogView(LogView logView) {
		this.logView = logView;
	}
	/**
	 *
	 */
	public void setPerson(Person person) {
		this.person = person;
	}
}
