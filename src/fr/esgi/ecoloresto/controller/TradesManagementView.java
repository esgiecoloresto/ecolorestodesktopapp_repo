package fr.esgi.ecoloresto.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

public class TradesManagementView implements Initializable{

	MainApp mainApp;
	LogView logView;

	@FXML
	ListView<String> lvTrades;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		String url = "http://ecolorestoapi.herokuapp.com/trades/";
		URLConnection conn = null;
		InputStream response = null;
		try {
			conn = new URL(url).openConnection();
			response = conn.getInputStream();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ObservableList<String> olTrades = FXCollections.observableArrayList();

		try (Scanner scanner = new Scanner(response)) {
			String responseBody = scanner.useDelimiter("\\A").next();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(responseBody);
			JSONArray jsonDishes = (JSONArray) obj;
			for(int i = 0; i < jsonDishes.size(); i++) {
				Object objDish = parser.parse(jsonDishes.get(i).toString());
				JSONObject jsonDish = (JSONObject) objDish;
				String name = (String) jsonDish.get("name");
				JSONObject jsonCreator = (JSONObject) jsonDish.get("auteur");
				JSONObject jsonCustomer = (JSONObject) jsonDish.get("client");
				System.out.println(jsonCreator.get("pseudo") + " a servi \"" + name + "\" à " + jsonCustomer.get("pseudo"));
				olTrades.add(jsonCreator.get("pseudo") + " a servi \"" + name + "\" à " + jsonCustomer.get("pseudo"));
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lvTrades.setItems(olTrades);
	}


	/**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    @FXML
	public void goToHome(){
		logView.loadAccount();
	}
}
