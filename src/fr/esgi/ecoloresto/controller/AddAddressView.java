package fr.esgi.ecoloresto.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class AddAddressView implements Initializable{

	@FXML
	private Button send;
	@FXML
	private TextField tfFirstName, tfLastName, tfAddr, tfAddrComp1, tfAddrComp2, tfZipCode, tfCity;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	/**
	 * GETTERS & SETTERS
	 */
	public Button getSend() {
		return send;
	}
	public void setSend(Button send) {
		this.send = send;
	}
	public TextField getTfFirstName() {
		return tfFirstName;
	}
	public void setTfFirstName(TextField tfFirstName) {
		this.tfFirstName = tfFirstName;
	}
	public TextField getTfLastName() {
		return tfLastName;
	}
	public void setTfLastName(TextField tfLastName) {
		this.tfLastName = tfLastName;
	}
	public TextField getTfAddr() {
		return tfAddr;
	}
	public void setTfAddr(TextField tfAddr) {
		this.tfAddr = tfAddr;
	}
	public TextField getTfAddrComp1() {
		return tfAddrComp1;
	}
	public void setTfAddrComp1(TextField tfAddrComp1) {
		this.tfAddrComp1 = tfAddrComp1;
	}
	public TextField getTfAddrComp2() {
		return tfAddrComp2;
	}
	public void setTfAddrComp2(TextField tfAddrComp2) {
		this.tfAddrComp2 = tfAddrComp2;
	}
	public TextField getTfZipCode() {
		return tfZipCode;
	}
	public void setTfZipCode(TextField tfZipCode) {
		this.tfZipCode = tfZipCode;
	}
	public TextField getTfCity() {
		return tfCity;
	}
	public void setTfCity(TextField tfCity) {
		this.tfCity = tfCity;
	}

}
