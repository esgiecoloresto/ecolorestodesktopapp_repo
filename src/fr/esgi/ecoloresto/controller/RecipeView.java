package fr.esgi.ecoloresto.controller;

import java.net.URL;
import java.util.ResourceBundle;

import fr.esgi.ecoloresto.model.Ingredient;
import fr.esgi.ecoloresto.model.Recipe;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class RecipeView implements Initializable{

	@FXML
	GridPane gpRecipe;
	@FXML
	Text ingredients;
	@FXML
	Label recipeTitle;

	Recipe recipe;


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		recipe = (Recipe) ContextAccount.getInstance().getContextObject("aRecipe");

		if(!recipe.getUrl_image().equals("")) {
			Image image = new Image(recipe.getUrl_image(), 190.0, 190.0, true, true);
	        ImageView imageView = new ImageView(image);
			gpRecipe.add(imageView, 1, 1);
		}

		recipeTitle.setText(recipe.getName() + " ("+recipe.getServing()+")");


		String ings = "Ingrédients : ";
		Ingredient[] ingtab = recipe.getIngredients();
		System.out.println(ingtab.length);
		for(int i = 0; i < ingtab.length; i++) {
			ings += ingtab[i].getName();
			if(i != ingtab.length-1)
				ings += ", ";
		}
		ingredients.setText(ings + "\n\nPreparation : " + recipe.getPreparation() + "\nCuisson : "+recipe.getBaking() + "\n\n" + recipe.getDirections());

	}

}
