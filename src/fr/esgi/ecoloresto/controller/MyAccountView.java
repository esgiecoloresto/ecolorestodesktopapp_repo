package fr.esgi.ecoloresto.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MyAccountView implements Initializable{

	MainApp mainApp;
	LogView logView;
	private Person person;
	long idNEwImg;
	String imageUrl;
	boolean isImgChanged;
	ImageView imageView;

	@FXML
	Text rating;
	@FXML
	private TextField tfOldPass, tfEmail, tfNewPass, tfConfirmPass;
	@FXML
	ListView<String> lvLastTrades;
	@FXML
	GridPane gpMyAccount;
	@FXML
	private Button sendModif;

	@SuppressWarnings("static-access")
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		idNEwImg = 0;
		isImgChanged = false;

		person = (Person) ContextAccount.getInstance().getContextObject("myPerson");

		rating.setText(person.getPseudo() + " " + person.getRating()+" / 5");
		tfEmail.setPromptText(person.getEmail());

		Image image = new Image(person.getUrl_image(), 200.0, 200.0, true, true);
		imageView = new ImageView(image);
		imageView.setEffect(new DropShadow(20, Color.BLACK));
		gpMyAccount.add(imageView, 0, 1, 3, 2);
		gpMyAccount.setHalignment(imageView, HPos.CENTER);




		lvLastTrades.setItems(getDishesForTrades());
		lvLastTrades.setFixedCellSize(40.0);

		sendModif.setOnAction(event -> {
			modifUser();
		});
	}

	public ObservableList<String> getDishesForTrades() {
		String url = "http://ecolorestoapi.herokuapp.com/trades/";
		URLConnection conn = null;
		InputStream response = null;
		try {
			conn = new URL(url).openConnection();
			response = conn.getInputStream();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ObservableList<String> olTrades = FXCollections.observableArrayList();

		if(response != null) {
			try (Scanner scanner = new Scanner(response)) {
				String responseBody = scanner.useDelimiter("\\A").next();
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(responseBody);
				JSONArray jsonDishes = (JSONArray) obj;
				for(int i = 0; i < jsonDishes.size(); i++) {
					Object objDish = parser.parse(jsonDishes.get(i).toString());
					JSONObject jsonDish = (JSONObject) objDish;
					String name = (String) jsonDish.get("name");
					JSONObject jsonCreator = (JSONObject) jsonDish.get("auteur");
					JSONObject jsonCustomer = (JSONObject) jsonDish.get("client");
					System.out.println(jsonCreator.get("pseudo") + " a servi \"" + name + "\" à " + jsonCustomer.get("pseudo"));

					if(person.getPseudo().equals(jsonCreator.get("pseudo"))) {
						if(jsonCustomer.get("pseudo").equals(""))
							olTrades.add("Vous avez proposé \"" + name + "\"");
						else
							olTrades.add("Vous avez servi \"" + name + "\" à " + jsonCustomer.get("pseudo"));
					}
					if(person.getPseudo().equals(jsonCustomer.get("pseudo")))
						olTrades.add(jsonCreator.get("pseudo") + "vous a servi \"" + name + "\"");
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return olTrades;
	}

	public void modifUser() {
		if(tfOldPass.getText().equals("")) {
			System.out.println("MOT DE PASSE !");
			// Create analert:
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Mot de passe oublié !\nIl est nécessaire à la validation des modifications apportées.");
			alert.showAndWait();
		} else {
			// Create the URL:
			String query = "http://ecolorestoapi.herokuapp.com/user/"+person.getPseudo();

			// Set Parametres
			Map<String,Object> params = new LinkedHashMap<>();
			params.put("email", (tfEmail.getText().equals("") ? person.getEmail() : tfEmail.getText()));
			person.setEmail((tfEmail.getText().equals("") ? person.getEmail() : tfEmail.getText()));
			params.put("oldPassword", getHashCode(tfOldPass.getText()));

			if(!tfNewPass.getText().equals(tfConfirmPass.getText())) {
				System.out.println("NEW PASS NON IDENTIQUES : "+tfNewPass.getText() + " "+ tfConfirmPass.getText());
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Information");
				alert.setHeaderText(null);
				alert.setContentText("Veuillez confirmer votre nouveau mot de passe.");
				alert.showAndWait();
			} else {

				if(isImgChanged) {
					System.out.println("Image is changed");
					sendImage();
					if(idNEwImg != 0) {
						System.out.println("In set params");
						params.put("image", idNEwImg);
						person.setUrl_image(imageUrl);
						idNEwImg = 0;
					}
				} else {
					System.out.println("NOPE");
				}

				if(tfNewPass.getText().equals("") && tfConfirmPass.getText().equals("")) {
					System.out.println("NEW PASS = OLD PASS : "+tfNewPass.getText() + " "+ tfConfirmPass.getText());
					params.put("newPassword", getHashCode(tfOldPass.getText()));
				} else {
					System.out.println("MOT DE PASSE CHANGE : "+tfNewPass.getText() + " "+ tfConfirmPass.getText());
					params.put("newPassword", getHashCode(tfNewPass.getText()));
				}




				// Replace blanks with HTML-Equivalent:
				query = query.replace(" ", "%20");
				System.out.println(query);

				URL url;
				try {
					url = new URL(query);

					StringBuilder postData = new StringBuilder();
					for (Map.Entry<String,Object> param : params.entrySet()) {
						if (postData.length() != 0) postData.append('&');
						postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
						postData.append('=');
						postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
					}
					System.out.println("POST DATA : "+postData.toString());
					byte[] postDataBytes = postData.toString().getBytes("UTF-8");

					HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
					httpCon.setRequestMethod("PUT");
					httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
					httpCon.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
					httpCon.setDoOutput(true);
					httpCon.getOutputStream().write(postDataBytes);

					System.out.println(httpCon.getResponseCode() + " :: " + httpCon.getResponseMessage());

					Reader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream(), "UTF-8"));

					StringBuilder sb = new StringBuilder();
					for (int c; (c = in.read()) >= 0;)
						sb.append((char)c);
					String response = sb.toString();
					System.out.println(response);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private StringBuffer getHashCode(String pass) {
		StringBuffer sb = new StringBuffer();

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			String str = pass;
			byte[] h = md.digest(str.getBytes(StandardCharsets.UTF_8));
			System.out.println(h.length+" ---- "+h.toString());
			for(int i = 0; i< h.length; i++)
				System.out.println(h[i]);

			System.out.println("______________________");

			for(int i = 0; i< h.length; i++)
				sb.append(Integer.toString((h[i] & 0xff) +0x100,16).substring(1));

			System.out.println(sb.toString());

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sb;
	}

	@FXML
	public void updateImg() throws IOException {

		final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(MainApp.getPrimaryStage());
        TextField tfUrl = new TextField("http://...");
        tfUrl.setPrefWidth(280);
        Button btnValid = new Button("Valider");
        btnValid.setAlignment(Pos.BOTTOM_CENTER);
        BorderPane aPane = new BorderPane(tfUrl, null, null, btnValid, null);
        aPane.setPrefSize(300, 150);
        aPane.setPadding(new Insets(30, 10, 30, 10));
        BorderPane.setAlignment(tfUrl, Pos.CENTER);
        BorderPane.setAlignment(btnValid, Pos.CENTER);
        Scene dialogScene = new Scene(aPane, 300, 150);
        dialog.setScene(dialogScene);
        dialog.setResizable(false);
        dialog.show();

        btnValid.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				if(!tfUrl.getText().equals("")) {
					imageUrl = tfUrl.getText();
					isImgChanged = true;
					imageView.setImage(new Image(imageUrl, 200.0, 200.0, true, true));
					dialog.close();
				}


			}
		});
	}

	public void sendImage() {
		URL url;
		try {
			url = new URL("http://ecolorestoapi.herokuapp.com/image");
			Map<String,Object> params = new LinkedHashMap<>();
			params.put("name", "avatar" + person.getPseudo());
			params.put("link", imageUrl);
			params.put("isPrincipalImage", true);

			StringBuilder postData = new StringBuilder();
			for (Map.Entry<String,Object> param : params.entrySet()) {
				if (postData.length() != 0) postData.append('&');
				postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
				postData.append('=');
				postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
			}
			byte[] postDataBytes = postData.toString().getBytes("UTF-8");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
			conn.setDoOutput(true);
			conn.getOutputStream().write(postDataBytes);
			Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

			StringBuilder sb = new StringBuilder();
			for (int c; (c = in.read()) >= 0;)
				sb.append((char)c);
			String response = sb.toString();
			System.out.println("##### = "+response);

			JSONParser parser = new JSONParser();
			Object obj = parser.parse(response);
			JSONObject jsonObj = (JSONObject) obj;
			System.out.println(jsonObj.get("idIMAGE"));
			idNEwImg = (long) jsonObj.get("idIMAGE");

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 *
	 * @param mainApp
	 */
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}
	/**
	 * Is called by the main application to give a reference back to itself.
	 *
	 * @param mainApp
	 */
	public void setLogView(LogView logView) {
		this.logView = logView;
	}
	//    /**
	//    *
	//    */
	//   public void setPerson(Person person) {
	//	   tfPseudo.setText(person.getPseudo());
	//	   rating.setText(person.getRating()+" / 5");
	////	   tfEmail.setText(person.getEmail());
	//
	//	   this.person = new Person(person.getId(), person.getPseudo(), person.getEmail(), person.getRating());
	//   }

	@FXML
	public void goToHome(){
		logView.loadAccount();
	}
}
