package fr.esgi.ecoloresto.controller;

import java.io.IOException;

import fr.esgi.ecoloresto.MainApp;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class AdminAccountView {

	MainApp mainApp;
	LogView logView;

	/**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }


    public void goToTradesManagement(){
    	try {
            // Load TradesManagementView.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/TradesManagementView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Set trades managment view to rootLayout.
            mainApp.rootLayout.setCenter(loginView);

            // Give the controller access to the main app.
            TradesManagementView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public void goToUserManagement(){
		try {
            // Load UserManagement view.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/UserManagementView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Set UserManagement view to rootLayout.
            mainApp.rootLayout.setCenter(loginView);

            // Give the controller access to the main app.
            UserManagementView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void goToWaitingRecipes(){
		try {
            // Load WaitingRecipe view.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/WaitingRecipesView.fxml"));
            AnchorPane loginView = (AnchorPane) loader.load();

            // Set WaitingRecipe view to rootLayout.
            mainApp.rootLayout.setCenter(loginView);

            // Give the controller access to the main app.
            WaitingRecipesView controller = loader.getController();
            controller.setMainApp(mainApp);
            controller.setLogView(logView);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
}
