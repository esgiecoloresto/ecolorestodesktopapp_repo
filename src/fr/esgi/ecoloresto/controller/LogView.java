package fr.esgi.ecoloresto.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Dish;
import fr.esgi.ecoloresto.model.Person;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class LogView implements Initializable {

	@FXML
	private TextField nicknameSubscribe, mailSubscribe, nicknameLogin;
	@FXML
	private PasswordField passSubscribe, confirmSubscribe, passLogin;
	@FXML
	private ImageView imgViewLogo;

	// Reference to the main application.
	private MainApp mainApp;
	private Person person;
	private Boolean isAdmin;

	@FXML
	public void login() {
		if(nicknameLogin.getText().equals("") || passLogin.getText().equals("")) {
			// Alert if Login or Password are empty
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Veuillez saisir votre pseudo et votre mot de passe.");
			alert.showAndWait();
		} else {
			// GET /user
			String url = "http://ecolorestoapi.herokuapp.com/user/";

			URLConnection connection = null;
			InputStream response = null;
			try {
				connection = new URL(url).openConnection();

				connection.setRequestProperty("Pseudo", nicknameLogin.getText());
				connection.setUseCaches(false);
				connection.setRequestProperty("Password", getHashCode(passLogin.getText()).toString());
				System.setProperty("http.keepAlive", "false");

				response = connection.getInputStream();
			} catch (IOException e1) {
				// Alert if Login or Password are empty
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Information");
				alert.setHeaderText(null);
				alert.setContentText("Le mot de passe est faux ou le compte n'existe pas.");
				alert.showAndWait();
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// Response from request
			try (Scanner scanner = new Scanner(response)) {
				String responseBody = scanner.useDelimiter("\\A").next();

				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(responseBody);
					JSONObject jsonObj = (JSONObject) obj;
					JSONObject jsonImage = (JSONObject) jsonObj.get("image");

					isAdmin = (Boolean) jsonObj.get("isAdmin");

					person = new Person(0, (String) jsonObj.get("pseudo"), (String) jsonObj.get("email"), 4, (String) jsonImage.get("link"));
					person.setRating((((String) jsonObj.get("vote")) == null)?0:Integer.parseInt((String) jsonObj.get("vote")));
					JSONArray jsonDishes = (JSONArray) jsonObj.get("currentDishes");

					// Set dishes to Person
					Dish[] dishes = new Dish[jsonDishes.size()];
					for(int i = 0; i < jsonDishes.size() && i < 10; i++) {
						Object objDish = parser.parse(jsonDishes.get(i).toString());
						JSONObject jsonDish = (JSONObject) objDish;

						dishes[i] = new Dish(i,(String) jsonDish.get("name"),(String) jsonDish.get("dishDescription"));
					}
					if(dishes != null)
						person.setDishes(dishes);
				}catch(ParseException pe) {
					System.out.println("position: " + pe.getPosition());
					System.out.println(pe);
				}
			}

			// Save Person
			ContextAccount.getInstance().addContextObject("myPerson", person);
			loadAccount();
		}
	}

	@FXML
	public void subscribe() {

		// Check account

		if(nicknameSubscribe.getText().equals("")) {
			// Alert if Login is empty
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Veuillez saisir un pseudo.");
		} else if(!passSubscribe.getText().equals(confirmSubscribe.getText()) && !passSubscribe.getText().equals("")) {
			// Alert if Login or Password are empty
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Veuillez saisir un mot de passe et le confirmer.");
		} else if (mailSubscribe.getText().equals("")) {
			// Alert if Login or Password are empty
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Veuillez saisir une adresse email.");
		} else {

			URL url;
			try {
				url = new URL("http://ecolorestoapi.herokuapp.com/user");

				Map<String,Object> params = new LinkedHashMap<>();
				params.put("pseudo", nicknameSubscribe.getText());
				params.put("email", mailSubscribe.getText());
				params.put("password", getHashCode(passSubscribe.getText()));

				StringBuilder postData = new StringBuilder();
				for (Map.Entry<String,Object> param : params.entrySet()) {
					if (postData.length() != 0) postData.append('&');
					postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
					postData.append('=');
					postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
				}
				byte[] postDataBytes = postData.toString().getBytes("UTF-8");

				HttpURLConnection conn = (HttpURLConnection)url.openConnection();
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
				conn.setDoOutput(true);
				conn.getOutputStream().write(postDataBytes);
				Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

				StringBuilder sb = new StringBuilder();
				for (int c; (c = in.read()) >= 0;)
					sb.append((char)c);
				String response = sb.toString();
				//System.out.println("##### = "+response);


				JSONParser parser = new JSONParser();
				try{

					Object obj = parser.parse(response);
					JSONObject jsonObj = (JSONObject) obj;

					isAdmin = (Boolean) jsonObj.get("isAdmin");

					person = new Person(0, (String) jsonObj.get("pseudo"), (String) jsonObj.get("email"), 4, (long) jsonObj.get("IMAGE_id"));
					person.setRating(0);

				}catch(ParseException pe) {
					System.out.println("position: " + pe.getPosition());
					System.out.println(pe);
				}

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ContextAccount.getInstance().addContextObject("myPerson", person);

			// Alert if success
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Votre compte a bien été créé, veuillez vous connecter.");
			alert.showAndWait();

			nicknameLogin.setText(nicknameSubscribe.getText());

		}
	}

	public void loadAccount() {
		if(isAdmin) {
			try {
				// Load Simple user view.
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("view/AdminAccountView.fxml"));
				AnchorPane loginView = (AnchorPane) loader.load();

				// Set view in rootLayout.
				mainApp.rootLayout.setCenter(loginView);

				// Give MainApp access to controller
				AdminAccountView controller = loader.getController();
				controller.setMainApp(mainApp);
				controller.setLogView(this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				// Load Admin user view.
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(MainApp.class.getResource("view/AccountView.fxml"));
				AnchorPane loginView = (AnchorPane) loader.load();

				// Set view in rootLayout.
				mainApp.rootLayout.setCenter(loginView);

				// Give MainApp access to controller
				AccountView controller = loader.getController();
				controller.setMainApp(mainApp);
				controller.setLogView(this);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private StringBuffer getHashCode(String pass) {
		StringBuffer sb = new StringBuffer();

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			String str = pass;
			byte[] h = md.digest(str.getBytes(StandardCharsets.UTF_8));

			for(int i = 0; i< h.length; i++)
				sb.append(Integer.toString((h[i] & 0xff) +0x100,16).substring(1));

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return sb;
	}


	/**
	 * Is called by the main application to give a reference back to itself.
	 *
	 * @param mainApp
	 */
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	public Person getPerson() {
		return this.person;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		System.out.println("TEST");
	}
}
