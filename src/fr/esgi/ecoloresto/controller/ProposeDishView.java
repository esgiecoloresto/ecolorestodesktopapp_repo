package fr.esgi.ecoloresto.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Address;
import fr.esgi.ecoloresto.model.Counterparty;
import fr.esgi.ecoloresto.model.Person;
import fr.esgi.ecoloresto.model.TypeCounterparty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ProposeDishView implements Initializable{
	MainApp mainApp;
	LogView logView;
	int nbImg;
	String url1, url2, url3;
	private Person person;
	ObservableList<Address> olAddresses = FXCollections.observableArrayList();
	ObservableList<String> olAddressesStr = FXCollections.observableArrayList();

	ObservableList<TypeCounterparty> olType = FXCollections.observableArrayList();
	ObservableList<String> olTypeStr = FXCollections.observableArrayList();

	ObservableList<Counterparty> olTypeService = FXCollections.observableArrayList();
	ObservableList<String> olTypeServiceStr = FXCollections.observableArrayList();

	ObservableList<Counterparty> olTypeCash = FXCollections.observableArrayList();
	ObservableList<String> olTypeCashStr = FXCollections.observableArrayList();

	ObservableList<Counterparty> olTypeMetirial = FXCollections.observableArrayList();
	ObservableList<String> olTypeMetirialStr = FXCollections.observableArrayList();

	ObservableList<Counterparty> olCtpForDish = FXCollections.observableArrayList();
	ObservableList<String> olCptStr = FXCollections.observableArrayList();

	@FXML
	VBox vbox1, vbox2, vbox3;
	@FXML
	TextArea taAdvice, taDescription;
	@FXML
	TextField tfName;
	@FXML
	Button addImg;
	@FXML
	ChoiceBox<String> cbAddresses;
	@FXML
	ListView<String> lvCounterparty;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		nbImg = 0;
		lvCounterparty.setItems(olCptStr);

		// Init action of button add image
		addImg.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				Button button = (Button) event.getSource();

				if(button.getText().equals("+ Image")) {
					switch (nbImg) {
					case 0:
						vbox1.setDisable(false);
						TextField tf1 = new TextField("http://");
						vbox1.getChildren().add(tf1);
						break;
					case 1:
						vbox2.setDisable(false);
						TextField tf2 = new TextField("http://");
						vbox2.getChildren().add(tf2);
						break;
					case 2:
						vbox3.setDisable(false);
						TextField tf3 = new TextField("http://");
						vbox3.getChildren().add(tf3);
						break;
					default:
						break;
					}
					addImg.setText("Valider");
				} else {
					switch(nbImg) {

					case 2 :
						if(vbox3.getChildren().size() == 1) {
							TextField tf3 = (TextField) vbox3.getChildren().get(0);
							url3 = tf3.getText();
							Image image3 = new Image(url3, 120.0, 120.0, true, true);
							System.out.println(image3.toString());
		    		        ImageView imageView3 = new ImageView(image3);
		    		        vbox3.getChildren().add(0, imageView3);
		    		        addImg.setText("Modifier");
						} else {
							TextField tf3 = (TextField) vbox3.getChildren().get(1);
							ImageView imageView3 = (ImageView) vbox3.getChildren().get(0);
							if(!tf3.getText().equals(url3)) {
								Image image3 = new Image(tf3.getText(), 120.0, 120.0, true, true);
								imageView3.setImage(image3);
							}
						}

					case 1 :
						if(vbox2.getChildren().size() == 1) {
							TextField tf2 = (TextField) vbox2.getChildren().get(0);
							url2 = tf2.getText();
							Image image2 = new Image(url2, 120.0, 120.0, true, true);
							System.out.println(image2.toString());
		    		        ImageView imageView2 = new ImageView(image2);
		    		        vbox2.getChildren().add(0, imageView2);
		    		        addImg.setText("+ Image");
		    		        nbImg++;
						} else {
							TextField tf2 = (TextField) vbox2.getChildren().get(1);
							ImageView imageView2 = (ImageView) vbox2.getChildren().get(0);
							if(!tf2.getText().equals(url2)) {
								Image image2 = new Image(tf2.getText(), 120.0, 120.0, true, true);
								imageView2.setImage(image2);
							}
						}

					case 0:
						if(vbox1.getChildren().size() == 1) {
							TextField tf1 = (TextField) vbox1.getChildren().get(0);
							url1 = tf1.getText();
							Image image1 = new Image(url1, 120.0, 120.0, true, true);
							System.out.println(image1.toString());
		    		        ImageView imageView1 = new ImageView(image1);
		    		        vbox1.getChildren().add(0, imageView1);
		    		        addImg.setText("+ Image");
		    		        nbImg++;
						} else {
							TextField tf1 = (TextField) vbox1.getChildren().get(1);
							ImageView imageView1 = (ImageView) vbox1.getChildren().get(0);
							if(!tf1.getText().equals(url1)) {
								Image image1 = new Image(tf1.getText(), 120.0, 120.0, true, true);
								imageView1.setImage(image1);
							}
						}
	    		        break;
					}
				}
			}
		});

		person = (Person) ContextAccount.getInstance().getContextObject("myPerson");
		setAddressesToList();
		cbAddresses.setItems(olAddressesStr);
		setCounterpartyToList();
	}

	public void setAddressesToList() {
		String url = "http://ecolorestoapi.herokuapp.com/addresses/" + person.getPseudo();
		URLConnection conn = null;
		InputStream response = null;
		try {
			conn = new URL(url).openConnection();
			response = conn.getInputStream();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try (Scanner scanner = new Scanner(response)) {
			String responseBody = scanner.useDelimiter("\\A").next();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(responseBody);
			JSONObject jsonAddressesOwner = (JSONObject) obj;

			JSONArray jsonAddresses = (JSONArray) jsonAddressesOwner.get("addresses");
			for(int i = 0; i < jsonAddresses.size(); i++) {
				Object objAddress = parser.parse(jsonAddresses.get(i).toString());
				JSONObject jsonAddress = (JSONObject) objAddress;

				olAddresses.add(new Address((long) jsonAddress.get("idADDRESS"), (String) jsonAddress.get("firstName"), (String) jsonAddress.get("lastName"), (String) jsonAddress.get("address"), (String) jsonAddress.get("addressComplement1"), (String) jsonAddress.get("addressComplement2"), (String) jsonAddress.get("zipCode"), (String) jsonAddress.get("city")));
				olAddressesStr.add((String) jsonAddress.get("firstName") + " " + (String) jsonAddress.get("lastName") + ", " + (String) jsonAddress.get("address") + ((((String) jsonAddress.get("addressComplement1")).equals(""))?"":" "+(String) jsonAddress.get("addressComplement1")) + ((((String) jsonAddress.get("addressComplement2")).equals(""))?"":" "+(String) jsonAddress.get("addressComplement2")) + ", " + (String) jsonAddress.get("zipCode") + " " + (String) jsonAddress.get("city"));
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setCounterpartyToList() {
		String url = "http://ecolorestoapi.herokuapp.com/counterparties";
		URLConnection conn = null;
		InputStream response = null;
		try {
			conn = new URL(url).openConnection();
			response = conn.getInputStream();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try (Scanner scanner = new Scanner(response)) {
			String responseBody = scanner.useDelimiter("\\A").next();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(responseBody);
			JSONObject jsonObject = (JSONObject) obj;

			JSONArray jsonTypes = (JSONArray) jsonObject.get("typesCounterparties");
			for(int i = 0 ; i < jsonTypes.size() ; i++) {
				JSONObject jsonType = (JSONObject) jsonTypes.get(i);
				olType.add(new TypeCounterparty((long) jsonType.get("idTYPECOUNTERPARTY"), (String) jsonType.get("name")));
				olTypeStr.add((String) jsonType.get("name"));
			}

			JSONArray jsonCounterparties = (JSONArray) jsonObject.get("counterparties");
			for(int i = 0; i < jsonCounterparties.size(); i++) {
				Object objCounterparty = parser.parse(jsonCounterparties.get(i).toString());
				JSONObject jsonCounterparty = (JSONObject) objCounterparty;
				JSONObject jsonType = (JSONObject) jsonCounterparty.get("typecounterparty");

				switch ((int) ((long) jsonType.get("idTYPECOUNTERPARTY"))) {
				case 1:
					// Supprimer ctp pécunier, juste ajouter Reglement en especes
//					String str = jsonCounterparty.get("priceIfPecuniary").toString();
//					olTypeCash.add(new Counterparty((long) jsonCounterparty.get("idCOUNTERPARTY"), (String) jsonCounterparty.get("name"), Double.parseDouble(str) ));
//					olTypeCashStr.add((String) jsonCounterparty.get("name"));
					olTypeCashStr.add("Règlement en espèces");
					break;
				case 2:
					olTypeMetirial.add(new Counterparty((long) jsonCounterparty.get("idCOUNTERPARTY"), (String) jsonCounterparty.get("name")));
					olTypeMetirialStr.add((String) jsonCounterparty.get("name"));
					break;
				case 3:
					olTypeService.add(new Counterparty((long) jsonCounterparty.get("idCOUNTERPARTY"), (String) jsonCounterparty.get("name")));
					olTypeServiceStr.add((String) jsonCounterparty.get("name"));
					break;
				default:
					break;
				}
			}
			olTypeMetirialStr.add("Autre...");
			olTypeServiceStr.add("Autre...");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@FXML
	public void addAddress() throws IOException {

		// Instantiation de la popup
		Stage popupStage = new Stage(StageStyle.UNIFIED);
		FXMLLoader loader = new FXMLLoader();
		AnchorPane popupWdw = (AnchorPane) loader.load(AddAddressView.class.getResourceAsStream("/fr/esgi/ecoloresto/view/AddAddressView.fxml"));
		Scene popupScene = new Scene(popupWdw);
		popupStage.setScene(popupScene);
		popupStage.setResizable(false);
		popupStage.initOwner(MainApp.getPrimaryStage());
		popupStage.setTitle("Ajout d'une adresse.");

		// Controller de la popup
		AddAddressView controller = loader.getController();

		EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				/* TODO */
				if(controller.getTfFirstName().getText().equals("") || controller.getTfLastName().getText().equals("") || controller.getTfAddr().getText().equals("") || controller.getTfZipCode().getText().equals("") || controller.getTfCity().getText().equals("")) {
					// Alert if fields are empty
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Information");
					alert.setHeaderText(null);
					alert.setContentText("Veuillir remplir les champs essentiels");
					alert.showAndWait();
				} else {
					olAddresses.add(new Address(0, controller.getTfFirstName().getText(), controller.getTfLastName().getText(), controller.getTfAddr().getText(), controller.getTfAddrComp1().getText(), controller.getTfAddrComp2().getText(), controller.getTfZipCode().getText(), controller.getTfCity().getText()));
					olAddressesStr.add(controller.getTfFirstName().getText() + " " + controller.getTfLastName().getText() + ", " + controller.getTfAddr().getText() + (((controller.getTfAddrComp1().getText()).equals(""))?"":" "+controller.getTfAddrComp1().getText()) + (((controller.getTfAddrComp2().getText()).equals(""))?"":" "+controller.getTfAddrComp2().getText()) + ", " + controller.getTfZipCode().getText() + " " + controller.getTfCity().getText());
					System.out.println(controller.getTfFirstName() + " " + controller.getTfLastName());
					popupStage.close();
				}
			}
		};
		controller.getSend().setOnAction(eventHandler);
		popupStage.show();
	}


	@FXML
	public void addCounterparty() throws IOException {
		// Instantiation de la popup
		Stage popupStage = new Stage(StageStyle.UNIFIED);
		popupStage.initModality(Modality.APPLICATION_MODAL);
		popupStage.initOwner(MainApp.getPrimaryStage());
		FXMLLoader loader = new FXMLLoader();
		AnchorPane popupWdw = (AnchorPane) loader.load(AddCounterpartyView.class.getResourceAsStream("/fr/esgi/ecoloresto/view/AddCounterpartyView.fxml"));
		Scene popupScene = new Scene(popupWdw);
		popupStage.setScene(popupScene);
		popupStage.setResizable(false);
		popupStage.initOwner(MainApp.getPrimaryStage());
		popupStage.setTitle("Ajout d'une contrepartie.");

		// Controller de la popup
		AddCounterpartyView controller = loader.getController();
		controller.getTfValue().setVisible(false);
		controller.getTypeCounterparty().setItems(olTypeStr);
		controller.getTypeCounterparty().getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				int idx = (int) newValue;
				switch (idx) {
				case 0:
					controller.getCounterparty().setItems(olTypeCashStr);
					break;
				case 1:
					controller.getCounterparty().setItems(olTypeMetirialStr);
					break;
				case 2:
					controller.getCounterparty().setItems(olTypeServiceStr);
					break;

				default:
					break;
				}
			}
		});
		controller.getCounterparty().getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				int idx = (int) newValue;
				if(idx == controller.getCounterparty().getItems().size()-1)
					controller.getTfValue().setVisible(true);
				else
					controller.getTfValue().setVisible(false);
			}

		});

		EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				/* TODO */
				System.out.println("ADD INGREDIENT ");
				Counterparty cpt = null;
				switch (controller.getTypeCounterparty().getSelectionModel().getSelectedIndex()) {
				case 0:
					long l = Long.parseLong(controller.getTfValue().getText());
					cpt = new Counterparty(0, controller.getCounterparty().getSelectionModel().getSelectedItem(), l);
					cpt.setIdType(1);
					break;
				case 1:
					cpt = new Counterparty(0, (controller.getCounterparty().getSelectionModel().getSelectedIndex() != controller.getCounterparty().getItems().size()-1) ? controller.getCounterparty().getSelectionModel().getSelectedItem() : controller.getTfValue().getText());
					cpt.setIdType(2);
					break;
				case 2:
					cpt = new Counterparty(0, (controller.getCounterparty().getSelectionModel().getSelectedIndex() != controller.getCounterparty().getItems().size()-1) ? controller.getCounterparty().getSelectionModel().getSelectedItem() : controller.getTfValue().getText());
					cpt.setIdType(3);
					break;
				default:
					break;
				}
				olCptStr.add(cpt.getName());
				olCtpForDish.add(cpt);
				System.out.println(controller.getTypeCounterparty().getSelectionModel().getSelectedItem().toString());
				popupStage.close();
			}
		};
		controller.getSend().setOnAction(eventHandler);
		popupStage.show();
	}

	@FXML
	public void sendDish() {
		String missingArgs = "";
		if(cbAddresses.getSelectionModel().getSelectedIndex() == -1) {
			missingArgs += "\nAdresse manquante";
		}
		if(tfName.getText().equals("")) {
			missingArgs += "\nNom du plat manquant";
		}
		if(taAdvice.getText().equals(""))
			missingArgs += "\nConseils manquants";
		if(taDescription.getText().equals(""))
			missingArgs += "\nLa description du plat est manquante";
		if(vbox1.getChildren().size() != 2)
			missingArgs += "\nVeuillez mettre au moins 1 image";
		if(olCptStr.size() == 0)
			missingArgs += "\nVeuillez mettre au moins 1 contrepartie";
		if(!missingArgs.equals("")) {
			// Alert if Login or Password are empty
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Veuillez remplir les champs suivants :" + missingArgs);
			alert.showAndWait();
		} else {

			// Create POST args
			Map<String,Object> params = new LinkedHashMap<>();
			// Add address to args
			Address addr = olAddresses.get(cbAddresses.getSelectionModel().getSelectedIndex());
			if(addr.getId() == 0) {
				params.put("firstName", addr.getFirstName());
				params.put("lastName", addr.getLastName());
				params.put("address", addr.getAddress());
				params.put("addressComplement1", addr.getAddressComplement1());
				params.put("addressComplement2", addr.getAddressComplement2());
				params.put("zipCode", addr.getZipCode());
				params.put("city", addr.getCity());
			} else {
				params.put("ADDRESS_id", addr.getId());
			}
			params.put("name", tfName.getText());
			params.put("advices", taAdvice.getText());
			params.put("dishDescription", taDescription.getText());
			params.put("pseudo", person.getPseudo());


			URL url;
			try {
				url = new URL("http://ecolorestoapi.herokuapp.com/dish");
				StringBuilder postData = new StringBuilder();
				for (Map.Entry<String,Object> param : params.entrySet()) {
					if (postData.length() != 0) postData.append('&');
					postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
					postData.append('=');
					postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
				}
				byte[] postDataBytes = postData.toString().getBytes("UTF-8");

				HttpURLConnection conn = (HttpURLConnection)url.openConnection();
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
				conn.setDoOutput(true);
				conn.getOutputStream().write(postDataBytes);

				Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

				StringBuilder sb = new StringBuilder();
				for (int c; (c = in.read()) >= 0;)
					sb.append((char)c);
				String response = sb.toString();
				System.out.println("##### = "+response);

				// Get idDish from response
				JSONParser parser = new JSONParser();

				Object obj = parser.parse(response);
				JSONObject jsonObj = (JSONObject) obj;
				long idDish = (long) jsonObj.get("idDISH");

				sendCtpForDish(idDish);
				sendTimesForDish(idDish);

				if(vbox1.getChildren().size() == 2) {
					TextField tf = (TextField) vbox1.getChildren().get(1);
					sendImgForDish(idDish, tf.getText(), 1);
				}
				if(vbox2.getChildren().size() == 2) {
					TextField tf = (TextField) vbox2.getChildren().get(1);
					sendImgForDish(idDish, tf.getText(), 2);
				}
				if(vbox3.getChildren().size() == 2) {
					TextField tf = (TextField) vbox3.getChildren().get(1);
					sendImgForDish(idDish, tf.getText(), 3);
				}
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Alert if Login or Password are empty
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText("Votre plat a bien été envoyé.");
			alert.showAndWait();

			goToHome();
		}
	}

	private void sendImgForDish(long idDish, String link, int i) {

		URL url;
			try {
				url = new URL("http://ecolorestoapi.herokuapp.com/image");
				Map<String,Object> params = new LinkedHashMap<>();
				params.put("name", "imageDish"+i);
				params.put("link", link);
				params.put("isPrincipalImage", (i==1)?true:false);
				params.put("DISH_id", idDish);

				StringBuilder postData = new StringBuilder();
				for (Map.Entry<String,Object> param : params.entrySet()) {
					if (postData.length() != 0) postData.append('&');
					postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
					postData.append('=');
					postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
				}
				byte[] postDataBytes = postData.toString().getBytes("UTF-8");

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
				conn.setDoOutput(true);
				conn.getOutputStream().write(postDataBytes);

				System.out.println(conn.getResponseCode() + " : " + conn.getResponseMessage());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	@SuppressWarnings("unchecked")
	private void sendCtpForDish(long idDish) {
		String http = "http://ecolorestoapi.herokuapp.com/counterparties";
		HttpURLConnection urlConnection=null;
	    URL url;
	    DataOutputStream printout;
		try {
			url = new URL(http);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setUseCaches(false);
			urlConnection.setConnectTimeout(10000);
			urlConnection.setReadTimeout(10000);
			urlConnection.setRequestProperty("Content-Type","application/json");

		    urlConnection.connect();

			JSONObject jsonParams = new JSONObject();
			JSONArray jsonArray = new JSONArray();

			for(int i = 0 ; i < olCtpForDish.size() ; i++) {
				Counterparty cpt = olCtpForDish.get(i);
				JSONObject jsonCtp = new JSONObject();
				jsonCtp.put("name", cpt.getName());
				jsonCtp.put("isFavoriteCounterParty", (i == 0)?true:false);
				jsonCtp.put("TYPECOUNTERPARTY_id", cpt.getIdType());
				jsonCtp.put("DISH_id", idDish);
				if(cpt.getIdType() == 1) {
					jsonCtp.put("priceIfPecuniary", cpt.getPriceIfPecuniary());
				} else {
					jsonCtp.put("priceIfPecuniary", null);
				}

				jsonArray.add(jsonCtp);
			}
			jsonParams.put("counterparties", jsonArray);

			System.out.println(jsonParams);

			printout = new DataOutputStream(urlConnection.getOutputStream ());
			byte[] postDataBytes = jsonParams.toString().getBytes("UTF-8");
			printout.write(postDataBytes);
			printout.flush ();
			printout.close ();
			System.out.println(urlConnection.getResponseCode() + " - "  + urlConnection.getResponseMessage());

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	private void sendTimesForDish(long idDish) {

		String http = "http://ecolorestoapi.herokuapp.com/times";
		HttpURLConnection urlConnection=null;
	    URL url;
	    DataOutputStream printout;
		try {
			url = new URL(http);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(true);
			urlConnection.setRequestMethod("POST");
			urlConnection.setUseCaches(false);
			urlConnection.setConnectTimeout(10000);
			urlConnection.setReadTimeout(10000);
			urlConnection.setRequestProperty("Content-Type","application/json");

		    urlConnection.connect();

			JSONObject jsonParams = new JSONObject();
			JSONArray jsonArray = new JSONArray();

			DateFormat dateFormatDay = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat dateFormatHour = new SimpleDateFormat("HH:mm:ss");
			Date date = new Date();

			JSONObject depositeDate = new JSONObject();
			depositeDate.put("date", dateFormatDay.format(date));
			depositeDate.put("time", dateFormatHour.format(date));
			depositeDate.put("isDepositeDate", true);
			depositeDate.put("isEndDate", false);
			depositeDate.put("DISH_id", idDish);

			jsonArray.add(depositeDate);

			JSONObject endDate = new JSONObject();
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DATE, 1);
			endDate.put("date", dateFormatDay.format(cal.getTime()));
			endDate.put("time", dateFormatHour.format(cal.getTime()));
			endDate.put("isDepositeDate", false);
			endDate.put("isEndDate", true);
			endDate.put("DISH_id", idDish);

			jsonArray.add(endDate);

			JSONObject getDate = new JSONObject();
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(date);
			cal2.add(Calendar.HOUR, 2);
			getDate.put("date", dateFormatDay.format(cal2.getTime()));
			getDate.put("time", dateFormatHour.format(cal2.getTime()));
			getDate.put("isDepositeDate", false);
			getDate.put("isEndDate", false);
			getDate.put("DISH_id", idDish);

			jsonArray.add(getDate);


			jsonParams.put("times", jsonArray);
			System.out.println(jsonParams);

			printout = new DataOutputStream(urlConnection.getOutputStream ());
			byte[] postDataBytes = jsonParams.toString().getBytes("UTF-8");
			printout.write(postDataBytes);
			printout.flush ();
			printout.close ();
			System.out.println(urlConnection.getResponseCode() + " - "  + urlConnection.getResponseMessage());

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    @FXML
	public void goToHome(){
		logView.loadAccount();
	}

}
