package fr.esgi.ecoloresto.controller;

import java.util.HashMap;
import java.util.Map;

import fr.esgi.ecoloresto.model.Person;

public class ContextAccount {
	private static ContextAccount instance;
	public static ContextAccount getInstance(){
		return instance == null ? new ContextAccount() : instance;
	}


	private static Map<String, Object> contextObjects = new HashMap<String, Object>();

	private static Person person;

	public Map<String, Object> getContextObjects(){
		return contextObjects;
	}
	public Object getContextObject(String key){
		return contextObjects.get(key);
	}

	public void addContextObject(String key, Object value){
		contextObjects.put(key, value);
	}

	@SuppressWarnings("static-access")
	public Person getCurrentPerson() {
		return this.person;
	}
	@SuppressWarnings("static-access")
	public void setCurrentPerson(Person person) {
		this.person = person;
	}
}
