package fr.esgi.ecoloresto.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class UserManagementView implements Initializable{

	MainApp mainApp;
	LogView logView;
	CheckBox[] cb;

	ObservableList<Person> persons = FXCollections.observableArrayList();

	@FXML
	GridPane gpUsers;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		ObservableList<RowConstraints> contraints;
		contraints = gpUsers.getRowConstraints();
		System.out.println(contraints.size() + " : " + contraints.toString());

		String url = "http://ecolorestoapi.herokuapp.com/users/";

		URLConnection connection = null;
		InputStream response = null;
		try {
			connection = new URL(url).openConnection();
			response = connection.getInputStream();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try (Scanner scanner = new Scanner(response)) {
			String responseBody = scanner.useDelimiter("\\A").next();
			System.out.println(responseBody);


			JSONParser parser = new JSONParser();
			try{
				Object obj = parser.parse(responseBody);
				JSONArray jsonArrObj = (JSONArray) obj;
				cb = new CheckBox[jsonArrObj.size()];

				for(int i = 0; i < jsonArrObj.size(); i++) {
					JSONObject jsonUser = (JSONObject) jsonArrObj.get(i);
					JSONObject jsonUserImg = (JSONObject) jsonUser.get("image");

					persons.add(new Person(i, (String) jsonUser.get("pseudo"), (String) jsonUser.get("email"), 4, (String) jsonUserImg.get("link")));
					persons.get(i).setAdmin((boolean) jsonUser.get("isAdmin"));
					fillGrid(persons.get(i), i);

					if(i%6 == 0)
						gpUsers.getRowConstraints().add(contraints.get(0));
					else if(i%6 == 3)
						gpUsers.getRowConstraints().add(contraints.get(1));
				}

			}catch(ParseException pe) {
				System.out.println("position: " + pe.getPosition());
				System.out.println(pe);
			}
		}

//		for(int j = 0; j < contraints.size(); j++) {
//			gpUsers.getRowConstraints().add(contraints.get(j));
//			System.out.println("************** "+contraints.get(j));
//		}
	}

	@FXML
	public void deleteUsers() throws IOException {
		int nbDelete = 0;
		gpUsers.getChildren().clear();
		for(int i = 0; i < cb.length; i++) {
			if(cb[i].isSelected()) {
				nbDelete++;
				System.out.println("IN : " + persons.get(i).getPseudo());

				URL url = null;
				try {
				    url = new URL("http://ecolorestoapi.herokuapp.com/user/" + persons.get(i).getPseudo() + "/");
				} catch (MalformedURLException exception) {
				    exception.printStackTrace();
				}
				System.out.println(url);
				HttpURLConnection httpURLConnection = null;
				try {
				    httpURLConnection = (HttpURLConnection) url.openConnection();
				    httpURLConnection.setDoOutput(true);
				    httpURLConnection.setRequestMethod("DELETE");
				    System.out.println(httpURLConnection.getResponseCode());
				} catch (IOException exception) {
				    exception.printStackTrace();
				} finally {
				    if (httpURLConnection != null) {
				        httpURLConnection.disconnect();
				    }
				}

				persons.remove(i-nbDelete);
			} else {
				fillGrid(persons.get(i-nbDelete), i-nbDelete);
			}
		}

		if(nbDelete > 0) {
			for(int i = 0; i < persons.size(); i++) {

			}
		}
	}

	@SuppressWarnings("static-access")
	public void fillGrid(Person person, int i) {

		Image image = new Image(persons.get(i).getUrl_image(), 100, 100, true, true);
		ImageView imView = new ImageView(image);
		gpUsers.add(imView, i%3*3, 2*(i-i%3)/3, 1, 1);
		gpUsers.setHalignment(imView, HPos.CENTER);
		gpUsers.setValignment(imView, VPos.CENTER);
		System.out.println(i%3 + " : " + 2*(i-i%3)/3);

		Text tPseudo = new Text(persons.get(i).getPseudo());
		Text tEmail = new Text(persons.get(i).getEmail());
		Text tRate = new Text(persons.get(i).getRating() + "/5");
		VBox vbox = new VBox(10, tPseudo, tEmail, tRate);
		if(persons.get(i).isAdmin()) {
			Text tAdmin = new Text("ADMIN");
			vbox.getChildren().add(tAdmin);
		}
		gpUsers.add(vbox, i%3*3+1, 2*(i-i%3)/3);

		TextArea taInfos = new TextArea();
		taInfos.setMaxSize(230, 90);
		taInfos.setPrefSize(230, 90);
		taInfos.setMinSize(230, 90);
		gpUsers.add(taInfos, i%3*3, 2*((i - i%3)/3)+1, 3, 1);
		gpUsers.setHalignment(taInfos, HPos.LEFT);
		gpUsers.setValignment(taInfos, VPos.CENTER);

		CheckBox aCb = new CheckBox();
		gpUsers.add(aCb, i%3*3+2, 2*(i-i%3)/3);
		gpUsers.setValignment(aCb, VPos.TOP);
		cb[i] = aCb;
	}

	@FXML
	public void selectAll() {
		for(int i = 0; i < cb.length; i++)
			cb[i].setSelected(true);
	}

	@FXML
	public void unselectAll() {
		for(int i = 0; i < cb.length; i++)
			cb[i].setSelected(false);
	}


	/**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    @FXML
	public void goToHome(){
		logView.loadAccount();
	}

}
