package fr.esgi.ecoloresto.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.esgi.ecoloresto.MainApp;
import fr.esgi.ecoloresto.model.Ingredient;
import fr.esgi.ecoloresto.model.Person;
import fr.esgi.ecoloresto.model.Recipe;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WaitingRecipesView implements Initializable {
	MainApp mainApp;
	LogView logView;
	ArrayList<Recipe> alRecipes;

	@FXML
	GridPane gpRecipes;

	public static final String URL_SOURCE = "http://ecolorestoapi.herokuapp.com/recipes/";

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		alRecipes = new ArrayList<>();

		getDishes();

		fillGridPane();
	}


    void getDishes() {
    	/*** Create the request ***/
		// Create the URL:
		String query = URL_SOURCE;
		// Replace blanks with HTML-Equivalent:
		query = query.replace(" ", "%20");

		/***
		 * Make the request (This needs to be in a try-catch block because things can go wrong)
		 ***/
		try
		{
			// Turn the string into a URL object
			URL urlObject = new URL(query);
			// Open the stream (which returns an InputStream):
			InputStream in = urlObject.openStream();
			System.out.println(in);
			System.out.println("END");
			/** Now parse the data (the stream) that we received back ***/
			// Coming shortly since we need to set up a parser

			try (Scanner scanner = new Scanner(in)) {
			    String responseBody = scanner.useDelimiter("\\A").next();
			    System.out.println(responseBody);

			    JSONParser parser = new JSONParser();
			    try{
			    	Object obj = parser.parse(responseBody);

			    	JSONArray array = (JSONArray)obj;

			         System.out.println("The 2nd element of array");
			         System.out.println(array);
			         for(int i = 0; i < array.size(); i++) {
				         Object obj2 = parser.parse(array.get(i).toString());

				         JSONObject jsonObj = (JSONObject) obj2;

				         String name = (String) jsonObj.get("name");
				         System.out.println("<3 "+name);

				         JSONArray jsonImages = (JSONArray) jsonObj.get("images");
						String imgLink = "";

						for(int l = 0; l < jsonImages.size(); l++) {
							JSONObject image = (JSONObject) parser.parse(jsonImages.get(l).toString());
							if((boolean) image.get("isPrincipalImage")) {
								imgLink = image.get("link").toString();
								System.out.println("@@@@@@@@@ "+image.get("link"));
							}
						}

				         if(!(boolean) jsonObj.get("isAccepted")) {
				        	 alRecipes.add(new Recipe(6, (String) jsonObj.get("name"), (String) jsonObj.get("preparation"), (String) jsonObj.get("baking"), (String) jsonObj.get("serving"), (String) jsonObj.get("directions"), imgLink));

				        	 JSONObject jsonAuteur = (JSONObject) parser.parse(jsonObj.get("person").toString());
				        	 Person auteur = new Person((String) jsonAuteur.get("pseudo"));
				        	 alRecipes.get(alRecipes.size()-1).setPerson(auteur);

				        	 JSONArray ingredients = (JSONArray) parser.parse(jsonObj.get("ingredients").toString());
				        	 Ingredient[] ing = new Ingredient[ingredients.size()];
				        	 for(int l = 0; l < ingredients.size(); l++) {
				        		 JSONObject ingredient = (JSONObject) parser.parse(ingredients.get(l).toString());
				        		 ing[l] = new Ingredient(l, ingredient.get("name").toString());
				        	 }
				        	 alRecipes.get(alRecipes.size()-1).setIngredients(ing);

				         }
			         }
			      }catch(ParseException pe) {
			         System.out.println("position: " + pe.getPosition());
			         System.out.println(pe);
			      }
			}

		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
    }

	/**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param mainApp
     */
    public void setLogView(LogView logView) {
        this.logView = logView;
    }

    public void fillGridPane() {
    	ObservableList<RowConstraints> contraints = gpRecipes.getRowConstraints();
		System.out.println(gpRecipes.getRowConstraints());
		gpRecipes.getChildren().clear();
		gpRecipes.getRowConstraints().clear();
		for(int i = 0 ; i < alRecipes.size() ; i++) {
			Hyperlink hl = new Hyperlink(alRecipes.get(i).getName());
			final int iBis = i;
			hl.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					// Instantiation de la popup
					ContextAccount.getInstance().addContextObject("aRecipe", alRecipes.get(iBis));
					Stage popupStage = new Stage(StageStyle.UNIFIED);
					FXMLLoader loader = new FXMLLoader();
					ScrollPane popupWdw;
					try {
						popupWdw = (ScrollPane) loader.load(RecipeView.class.getResourceAsStream("/fr/esgi/ecoloresto/view/RecipeView.fxml"));
						Scene popupScene = new Scene(popupWdw);
						popupStage.setScene(popupScene);
						popupStage.initOwner(MainApp.getPrimaryStage());
						popupStage.setTitle("Détails de la recette");

						popupStage.show();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			gpRecipes.add(hl, 1, i*2);
			Button ok = new Button("OK");
			final int save = i;
			ok.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					System.out.println("RECETTE ACCEPTEE");
					// Create the URL:
					String query = "http://ecolorestoapi.herokuapp.com/recipe/";
					query += alRecipes.get(save).getName() + "/" + alRecipes.get(save).getPerson().getPseudo() + "/accept";
					// Replace blanks with HTML-Equivalent:
					query = query.replace(" ", "%20");
					System.out.println(query);

					URL url;
					try {
						url = new URL(query);
						HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
						httpCon.setDoOutput(true);
						httpCon.setRequestMethod("PUT");
						OutputStreamWriter out = new OutputStreamWriter(httpCon.getOutputStream());
						out.write("Resource content");
						out.close();
						httpCon.getInputStream();
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					alRecipes.remove(save);
					fillGridPane();
				}
			});
			gpRecipes.add(ok, 2, i*2);
			Button nok = new Button("NOK");
			nok.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					System.out.println("RECETTE REFUSEE");
					URL url = null;
					try {
					    url = new URL("http://ecolorestoapi.herokuapp.com/recipes/" + URLEncoder.encode(alRecipes.get(save).getName(), "UTF-8").replace("+", "%20") + "/" + URLEncoder.encode(alRecipes.get(save).getPerson().getPseudo(), "UTF-8").replace("+", "%20"));
					} catch (MalformedURLException exception) {
					    exception.printStackTrace();
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(url);
					String charset = "UTF-8";
					HttpURLConnection httpURLConnection = null;
					try {
					    httpURLConnection = (HttpURLConnection) url.openConnection();
					    httpURLConnection.setRequestProperty("Accept-Charset", charset);
					    httpURLConnection.setDoOutput(true);
					    httpURLConnection.setRequestMethod("DELETE");
					    System.out.println(httpURLConnection.getResponseCode() + " - " + httpURLConnection.getResponseMessage());
					} catch (IOException exception) {
					    exception.printStackTrace();
					} finally {
					    if (httpURLConnection != null) {
					        httpURLConnection.disconnect();
					    }
					}
					alRecipes.remove(save);
					fillGridPane();
				}
			});
			gpRecipes.add(nok, 3, i*2);

			if(!alRecipes.get(i).getUrl_image().equals("")) {
				Image image = new Image(alRecipes.get(i).getUrl_image(), 75.0, 75.0, true, true);
		        ImageView imageView = new ImageView(image);
				gpRecipes.add(imageView, 0, i*2, 1, 1);
			}

			Separator sep = new Separator(Orientation.HORIZONTAL);
			sep.setPrefSize(250, 10);
			sep.setHalignment(HPos.CENTER);

			gpRecipes.add(sep, 0, i*2+1, 4, 1);

			for(int j = 0; j < contraints.size(); j++)
				gpRecipes.getRowConstraints().add(contraints.get(j));
		}
    }

    @FXML
	public void goToHome(){
		logView.loadAccount();
	}
}
